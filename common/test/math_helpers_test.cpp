#include "math_helpers.hpp"

#include <map>

#include "gtest/gtest.h"
#include "operation.hpp"

const Point WorldSize(100, 101);
const Point Normal(50, 50);

enum class projections
{
    Minus,
    Zero,
    Plus
};
const std::map<projections, int> projectionsToInt{
    {projections::Minus, -1}, {projections::Zero, 0}, {projections::Plus, 1}};
const std::map<projections, size_t> normal_x_change{
    {projections::Minus, Normal.first - 1}, {projections::Zero, Normal.first}, {projections::Plus, Normal.first + 1}};
const std::map<projections, size_t> normal_y_change{{projections::Minus, Normal.second - 1},
                                                    {projections::Zero, Normal.second},
                                                    {projections::Plus, Normal.second + 1}};
const std::map<projections, size_t> zero_x_change{
    {projections::Minus, WorldSize.first - 1}, {projections::Zero, 0}, {projections::Plus, 1}};
const std::map<projections, size_t> zero_y_change{
    {projections::Minus, WorldSize.second - 1}, {projections::Zero, 0}, {projections::Plus, 1}};
const std::map<projections, size_t> max_x_change{
    {projections::Minus, WorldSize.first - 2}, {projections::Zero, WorldSize.first - 1}, {projections::Plus, 0}};
const std::map<projections, size_t> max_y_change{
    {projections::Minus, WorldSize.second - 2}, {projections::Zero, WorldSize.second - 1}, {projections::Plus, 0}};
std::map<Point, Point> MakePointDirectionMap(const std::map<projections, size_t>& x_change,
                                             const std::map<projections, size_t>& y_change)
{
    std::map<Point, Point> Direction_Expected;
    for (const auto& [x_projection, x_coord] : x_change)
        for (const auto& [y_projection, y_coord] : y_change) {
            if (x_projection == projections::Zero && y_projection == projections::Zero) {
                continue;
            }
            Point direction = std::make_pair(projectionsToInt.at(x_projection), projectionsToInt.at(y_projection));
            Point expected = std::make_pair(x_coord, y_coord);
            Direction_Expected.insert(std::make_pair(direction, expected));
        }
    return std::move(Direction_Expected);
}

TEST(MathHelpersTest, GetNextPositionNormal)
{
    Point NormalPoint(50, 50);
    const std::map<Point, Point> NormalPoint_Direction_Expected =
        std::move(MakePointDirectionMap(normal_x_change, normal_y_change));
    for (const auto& [direction, expected] : NormalPoint_Direction_Expected) {
        EXPECT_EQ(getNewPos(Normal, direction, WorldSize), expected);
    }
}

TEST(MathHelpersTest, GetNextPositionLeftBorder)
{
    const Point LeftBorder(50, 0);
    const std::map<Point, Point> LeftBorder_Direction_Expected =
        std::move(MakePointDirectionMap(normal_x_change, zero_y_change));
    for (auto& [direction, expected] : LeftBorder_Direction_Expected) {
        EXPECT_EQ(getNewPos(LeftBorder, direction, WorldSize), expected);
    }
}

TEST(MathHelpersTest, GetNextPositionUpBorder)
{
    const Point UpBorder(0, 50);
    const std::map<Point, Point> UpBorder_Direction_Expected =
        std::move(MakePointDirectionMap(zero_x_change, normal_y_change));
    for (auto& [direction, expected] : UpBorder_Direction_Expected) {
        EXPECT_EQ(getNewPos(UpBorder, direction, WorldSize), expected);
    }
}

TEST(MathHelpersTest, GetNextPositionRightBorder)
{
    const Point RightBorder(50, 100);
    const std::map<Point, Point> RightBorder_Direction_Expected =
        std::move(MakePointDirectionMap(normal_x_change, max_y_change));
    for (auto& [direction, expected] : RightBorder_Direction_Expected) {
        EXPECT_EQ(getNewPos(RightBorder, direction, WorldSize), expected);
    }
}

TEST(MathHelpersTest, GetNextPositionDownBorder)
{
    const Point DownBorder(99, 50);
    const std::map<Point, Point> DownBorder_Direction_Expected =
        std::move(MakePointDirectionMap(max_x_change, normal_y_change));
    for (auto& [direction, expected] : DownBorder_Direction_Expected) {
        EXPECT_EQ(getNewPos(DownBorder, direction, WorldSize), expected);
    }
}

TEST(MathHelpersTest, GetNextPositionLeftUpCorner)
{
    const Point LeftUpCorner(0, 0);
    const std::map<Point, Point> LeftUpCorner_Direction_Expected =
        std::move(MakePointDirectionMap(zero_x_change, zero_y_change));

    for (auto& [direction, expected] : LeftUpCorner_Direction_Expected) {
        EXPECT_EQ(getNewPos(LeftUpCorner, direction, WorldSize), expected);
    }
}

TEST(MathHelpersTest, GetNextPositionLeftDownCorner)
{
    const Point LeftDownCorner(99, 0);
    const std::map<Point, Point> LeftDownCorner_Direction_Expected =
        std::move(MakePointDirectionMap(max_x_change, zero_y_change));

    for (auto& [direction, expected] : LeftDownCorner_Direction_Expected) {
        EXPECT_EQ(getNewPos(LeftDownCorner, direction, WorldSize), expected);
    }
}

TEST(MathHelpersTest, GetNextPositionRightDownCorner)
{
    const Point RightDownCorner(99, 100);
    const std::map<Point, Point> RightDownCorner_Direction_Expected =
        std::move(MakePointDirectionMap(max_x_change, max_y_change));

    for (auto& [direction, expected] : RightDownCorner_Direction_Expected) {
        EXPECT_EQ(getNewPos(RightDownCorner, direction, WorldSize), expected);
    }
}

TEST(MathHelpersTest, GetNextPositionRightUpCorner)
{
    const Point RightUpCorner(0, 100);
    const std::map<Point, Point> RightUpCorner_Direction_Expected =
        std::move(MakePointDirectionMap(zero_x_change, max_y_change));

    for (auto& [direction, expected] : RightUpCorner_Direction_Expected) {
        EXPECT_EQ(getNewPos(RightUpCorner, direction, WorldSize), expected);
    }
}