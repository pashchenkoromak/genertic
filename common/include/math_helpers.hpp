#pragma once
#include <math.h>

#include <random>

typedef std::pair<int, int> Point;

// probability between 0.0 and 1.0
// https://stackoverflow.com/a/20309151
bool rand_bool(double prob);

const double EPS = 0.001;
bool double_eq(double a, double b, const double eps = EPS);

/**
 * Get random value [0, max)
 */
long long getRandValue(const long long max);

Point getNewPos(Point pos, Point direction, const Point& field_size);
