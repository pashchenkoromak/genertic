#ifndef WORLDCONFIGURATION_HPP
#define WORLDCONFIGURATION_HPP

#include <iostream>
#include <nlohmann/json.hpp>

constexpr size_t BITS_IN_GENOM = 8;

/// @brief Configuration with constants for world
class WorldConfiguration
{
public:
    WorldConfiguration(std::ifstream file);
    /// @brief Energy, that you can take when bite anyone.
    long double get_energy_for_bite() const;

    /// @brief Probability for children to mutate
    long double get_mutation_probability() const;

    /// @brief Fullness of world in the start.
    long double get_world_occupancy() const;

    /// @brief Default energy of any corpse.
    long double get_corpse_energy() const;

    /// @brief Speed of losing energy for corpses (in percentage from current energy).
    long double get_corpse_rotting() const;

    /// @brief Each actor must pay for living (in percentage from current energy).
    long double get_age_penalty() const;

    /// @brief Coefficient to multiple corpse energy on eating. So eater will take + corpse.energy *
    /// corpse_energy_penalty energy.
    long double get_corpse_energy_penalty() const;

    /// @brieg Energy, needed for waiting
    long double get_wait_energy() const;

    /// @brief Energy gotten by earth tillage (percentage from current earth energy).
    long double get_till_energy() const;

    /// @brief Energy for each child (percentage from current energy).
    long double get_energy_per_child() const;

    /// @brief Energy for photosyntesis. This will be multiplied to current x coord of actor.
    long double get_energy_for_photosyntesis() const;

    /// @brief Maximum possible earth energy at start. Note - this still random 0..earth_start_energy.
    long double get_earth_start_energy() const;

    /// @brief Maximum possible actor energy at start. Note - this still random 0..actor_start_energy.
    long double get_actor_start_energy() const;

    /// @brief Size of the world
    size_t get_n() const;
    size_t get_m() const;

    /// @brief Settings for heights
    long double get_min_height() const;
    long double get_max_height() const;
    long double get_min_height_photo_zero_multiplier() const;
    long double get_max_height_photo_zero_multiplier() const;
    long double get_pick_height_photo_multiplier() const;
    long double get_pick_multiplier() const;
    size_t get_smoothing_iterations() const;

    std::string get_colored_output_mode() const;
    std::string get_heights_mode() const;

private:
    nlohmann::json parsed;
    long double m_energy_for_bite;
    long double m_mutation_probability;
    long double m_world_occupancy;
    long double m_corpse_energy;
    long double m_corpse_rotting;
    long double m_age_penalty;
    long double m_corpse_energy_penalty;
    long double m_wait_energy;
    long double m_till_energy;
    long double m_energy_per_child;
    long double m_energy_for_photosyntesis;
    long double m_earth_start_energy;
    long double m_actor_start_energy;
    size_t m_n;
    size_t m_m;
    long double m_min_height;
    long double m_max_height;
    long double m_min_height_photo_zero_multiplier;
    long double m_max_height_photo_zero_multiplier;
    long double m_pick_height_photo_multiplier;
    long double m_pick_multiplier;
    size_t m_smoothing_iterations;
    std::string m_colored_output_mode;
    std::string m_heights_mode;
};

#endif // WORLDCONFIGURATION_HPP