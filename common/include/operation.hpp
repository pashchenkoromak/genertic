#include <functional>
#include <map>
#include <optional>
#include <set>
#include <string>
#include <vector>

#ifndef OPERATION_HPP
#define OPERATION_HPP

#include <bitset>

#include "worldConfiguration.hpp"

struct Length
{
    size_t val;
    Length(size_t v) : val(v) {}
    operator size_t() const { return val; }
};
struct Position
{
    size_t val;
    Position(size_t v) : val(v) {}
    operator size_t() const { return val; }
};

/// @namespace CommandLength
/// @brief CommandLength is a namespace with length of different commmands
namespace CommandLength
{
/// @brief After every GOTO command there are number of the next command
const Length GOTO = 8;

/// @brief Number of sensor (0 to 7)
const Length SENSOR_STATUS_ID = 3;

const Length VARIABLE_ID = 3;

/// @brief number const
const Length NUMBER = 4;

/// @brief Operation
/// @note Be care, when add new operations. If there is not enough, some new can
/// be ignored!
const Length OPERATION = 4;

/// @brief Direction
const Length DIRECTION = 3;

/// @brief Bool command (<, >, ==, &&, etc).
const Length BOOL = 4;

/// @brief Math ariphmetic command (+, -, *, /, %, etc).
const Length MATH = 4;

/// @brief true or false
const Length CONST_BOOL = 1;
} // namespace CommandLength

/// @enum touchSensorStatus
/// @brief list of possible touch sensors statuses by world
enum class touchSensorStatus : uint8_t
{
    /// @brief Nothing
    Empty,
    /// @brief Some actor
    Actor,
    /// @brief Some corpse
    Corpse,
    /// @brief At least 2 actors (maybe corpses)
    Mess
};

const std::map<touchSensorStatus, std::string> touchSensorStatusToString{
    {touchSensorStatus::Empty, "Empty"},
    {touchSensorStatus::Actor, "Actor"},
    {touchSensorStatus::Corpse, "Corpse"},
    {touchSensorStatus::Mess, "Mess"},
};

/// @enum eDirection
/// @brief includes 8 eDirection
enum class eDirection : uint8_t
{
    LEFT,
    RIGHT,
    UP,
    DOWN,
    UP_LEFT,
    DOWN_LEFT,
    UP_RIGHT,
    DOWN_RIGHT
};
const std::set directions{eDirection::LEFT,    eDirection::RIGHT,     eDirection::UP,       eDirection::DOWN,
                          eDirection::UP_LEFT, eDirection::DOWN_LEFT, eDirection::UP_RIGHT, eDirection::DOWN_RIGHT};
const std::map<eDirection, std::string> directionsToString{
    {eDirection::LEFT, "LEFT"},         {eDirection::RIGHT, "RIGHT"},          {eDirection::UP, "UP"},
    {eDirection::DOWN, "DOWN"},         {eDirection::UP_LEFT, "UP_LEFT"},      {eDirection::DOWN_LEFT, "DOWN_LEFT"},
    {eDirection::UP_RIGHT, "UP_RIGHT"}, {eDirection::DOWN_RIGHT, "DOWN_RIGHT"}};
const std::map<std::string, eDirection> stringToDirections{
    {"LEFT", eDirection::LEFT},         {"RIGHT", eDirection::RIGHT},          {"UP", eDirection::UP},
    {"DOWN", eDirection::DOWN},         {"UP_LEFT", eDirection::UP_LEFT},      {"DOWN_LEFT", eDirection::DOWN_LEFT},
    {"UP_RIGHT", eDirection::UP_RIGHT}, {"DOWN_RIGHT", eDirection::DOWN_RIGHT}};
const std::map<eDirection, std::pair<int, int>> directionsToPoint{
    {eDirection::LEFT, {0, -1}},     {eDirection::RIGHT, {0, 1}},     {eDirection::UP, {-1, 0}},
    {eDirection::DOWN, {1, 0}},      {eDirection::UP_LEFT, {-1, -1}}, {eDirection::DOWN_LEFT, {1, -1}},
    {eDirection::UP_RIGHT, {-1, 1}}, {eDirection::DOWN_RIGHT, {1, 1}}};

std::vector<std::bitset<BITS_IN_GENOM>> IntToBitVector(size_t type, size_t length);

/// @enum MathOperator
/// @brief math commands
/// @note In math expressions used prefix notation: + 3 4, for example.
enum class MathOperator : uint8_t
{
    PLUS = 0,
    MINUS = 1,
    NUMBER_CONST = 2,
    MULTIPLE = 3,
    DIVIDE = 4,
    REST_DIVIDE = 5,
    /// @brief getting energy of actor
    ENERGY = 6,
    /// @brief sensor status
    SENSOR_STATUS = 7,
    VARIABLE = 8
};
const std::set<MathOperator> MathOperators{
    MathOperator::PLUS,     MathOperator::MINUS,         MathOperator::NUMBER_CONST,
    MathOperator::MULTIPLE, MathOperator::DIVIDE,        MathOperator::REST_DIVIDE,
    MathOperator::ENERGY,   MathOperator::SENSOR_STATUS, MathOperator::VARIABLE};

const std::map<MathOperator, std::string> MathOperatorToString{
    {MathOperator::PLUS, "+"},        {MathOperator::MINUS, "-"},
    {MathOperator::NUMBER_CONST, ""}, {MathOperator::ENERGY, ""},
    {MathOperator::MULTIPLE, "*"},    {MathOperator::DIVIDE, "/"},
    {MathOperator::REST_DIVIDE, "%"}, {MathOperator::SENSOR_STATUS, "sensor"},
    {MathOperator::VARIABLE, "var"},

};
const std::map<MathOperator, std::function<int(int, int)>> MathOperatorTwoNumerics{
    {MathOperator::PLUS, [](int a, int b) { return a + b; }},
    {MathOperator::MINUS, [](int a, int b) { return a - b; }},
    {MathOperator::MULTIPLE, [](int a, int b) { return a * b; }},
    {MathOperator::DIVIDE,
     [](int a, int b) {
         if (b == 0) {
             return 0;
         } else {
             return a / b;
         };
     }},
    {MathOperator::REST_DIVIDE, [](int a, int b) {
         if (b == 0) {
             return 0;
         } else {
             return a % b;
         }
     }}};
bool IsReceivingTwoNumerics(const MathOperator& op);
/// @enum LogicalOperator
/// @brief Operator that return bool
enum class LogicalOperator : uint8_t
{
    BOOL_CONST,
    LESS,
    MORE,
    EQUAL_NUMERIC,
    NO_EQUAL_NUMERIC,
    EQUAL_BOOLS,
    NO_EQUAL_BOOLS,
    EQUAL_SENSORS,
    NO_EQUAL_SENSORS,
    NO,
    LESS_EQUAL,
    MORE_EQUAL,
    AND,
    OR
};

const std::map<LogicalOperator, std::function<bool(int, int)>> LogicalOperatorTwoNumerics{
    {LogicalOperator::LESS, [](int a, int b) { return a < b; }},
    {LogicalOperator::MORE, [](int a, int b) { return a > b; }},
    {LogicalOperator::EQUAL_NUMERIC, [](int a, int b) { return a == b; }},
    {LogicalOperator::NO_EQUAL_NUMERIC, [](int a, int b) { return a != b; }},
    {LogicalOperator::LESS_EQUAL, [](int a, int b) { return a <= b; }},
    {LogicalOperator::MORE_EQUAL, [](int a, int b) { return a >= b; }}};
const std::map<LogicalOperator, std::function<bool(touchSensorStatus, touchSensorStatus)>> LogicalOperatorTwoSensors{
    {LogicalOperator::EQUAL_SENSORS, [](touchSensorStatus a, touchSensorStatus b) { return a == b; }},
    {LogicalOperator::NO_EQUAL_SENSORS, [](touchSensorStatus a, touchSensorStatus b) { return a != b; }}};
const std::map<LogicalOperator, std::function<bool(bool, bool)>> LogicalOperatorTwoBools{
    {LogicalOperator::AND, [](bool a, bool b) { return a && b; }},
    {LogicalOperator::OR, [](bool a, bool b) { return a || b; }},
    {LogicalOperator::EQUAL_BOOLS, [](bool a, bool b) { return a == b; }},
    {LogicalOperator::NO_EQUAL_BOOLS, [](bool a, bool b) { return a != b; }},
};
const std::map<LogicalOperator, std::function<bool(bool)>> LogicalOperatorOneBool{
    {LogicalOperator::NO, [](bool a) { return !a; }}};

bool IsLogicalOperator(const LogicalOperator&);
bool IsReceivingTwoNumerics(const LogicalOperator&);
bool IsReceivingOneBool(const LogicalOperator&);
bool IsReceivingTwoBools(const LogicalOperator&);
bool IsReceivingTwoSensors(const LogicalOperator&);

const std::map<LogicalOperator, std::string> LogicalOperatorToString{
    {LogicalOperator::BOOL_CONST, "GetBoolConst"},
    {LogicalOperator::LESS, "<"},
    {LogicalOperator::MORE, ">"},
    {LogicalOperator::EQUAL_BOOLS, "=="},
    {LogicalOperator::EQUAL_NUMERIC, "=="},
    {LogicalOperator::EQUAL_SENSORS, "=="},
    {LogicalOperator::NO_EQUAL_BOOLS, "!="},
    {LogicalOperator::NO_EQUAL_NUMERIC, "!="},
    {LogicalOperator::NO_EQUAL_SENSORS, "!="},
    {LogicalOperator::NO, "NO"},
    {LogicalOperator::LESS_EQUAL, "<="},
    {LogicalOperator::MORE_EQUAL, ">="},
    {LogicalOperator::AND, "AND"},
    {LogicalOperator::OR, "OR"},
};

/// @enum operationType
/// @brief enum of possible main operations
enum class OperationType : uint8_t
{
    /// @brief GOTO uses number after itself, set next command position to this
    /// number. GOTO 134 // set next reading bit to 134.
    GOTO = 0,

    /// @brief IF checks some bool expression. And if it is true - goto
    /// first goto number, else - to the second Example (c++ code, ofc):
    /// A: photosyntesis
    /// if (Energy < 20)
    ///     goto A;
    /// else
    ///     goto B;
    /// B: make_child
    IF = 1,

    /// @brief is a command to getting energy by the sun
    PHOTOSYNTESIS = 2,

    /// @brief go in some direction (it looks in genom as GO DIRECTION)
    GO = 3,
    /// @brief Do nothing
    WAIT = 4,
    /// @brief Command to create a child to the world. For now it's R-strategy, so
    /// dont need additional info
    MAKE_CHILD = 5,
    /// @brief try to get energy from the earth under actor
    TILL = 6,
    /// @brief I'm not sure it must be here, but it's possible.
    DIE = 7,
    CHEMICAL_REACTION = 8,
    ASSIGN = 9,
};
const std::set<OperationType> OperationTypes{
    OperationType::GOTO,  OperationType::IF,   OperationType::PHOTOSYNTESIS,
    OperationType::GO,    OperationType::WAIT, OperationType::MAKE_CHILD,
    OperationType::TILL,  OperationType::DIE,  OperationType::CHEMICAL_REACTION,
    OperationType::ASSIGN};
const std::map<OperationType, std::string> OperationTypeToString{
    {OperationType::GOTO, ""},
    {OperationType::IF, "IF"},
    {OperationType::PHOTOSYNTESIS, "PHOTOSYNTESIS"},
    {OperationType::GO, "GO"},
    {OperationType::WAIT, "WAIT"},
    {OperationType::MAKE_CHILD, "MAKE_CHILD"},
    {OperationType::TILL, "TILL"},
    {OperationType::DIE, "DIE"},
    {OperationType::CHEMICAL_REACTION, "CHEMICAL_REACTION"},
    {OperationType::ASSIGN, "ASSIGN"}};

/// @struct Operation
/// @brief operation has a type and optional parameters.
struct Operation
{
    /// @brief default constructor and destructor
    /// @{
    Operation() = default;
    ~Operation() = default;
    /// @}

    /// @brief creates operation of given type
    /// @param[in] type - type of new Operation
    Operation(const OperationType& type);

    /// @brief Check, if it's a command to the world or no.
    bool IsWorldOperation();

    /// @brief Convert the Operation to human readable string
    /// @return the string
    std::string ToString() const;

    /// @brief type of Operation
    OperationType type_;
    std::optional<eDirection> direction_;
};

bool operator==(const Operation& lhs, const Operation& rhs);
bool operator!=(const Operation& lhs, const Operation& rhs);

#endif // OPERATION_HPP
