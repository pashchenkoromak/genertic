#include "worldConfiguration.hpp"

#include <fstream>

#include "operation.hpp"

WorldConfiguration::WorldConfiguration(std::ifstream file)
{
    parsed = nlohmann::json::parse(file);
    m_energy_for_bite = parsed["energy_for_bite"];
    m_mutation_probability = parsed["mutation_probability"];
    m_world_occupancy = parsed["world_occupancy"];
    m_corpse_energy = parsed["corpse_energy"];
    m_corpse_rotting = parsed["corpse_rotting"];
    m_age_penalty = parsed["age_penalty"];
    m_corpse_energy_penalty = parsed["corpse_energy_penalty"];
    m_wait_energy = parsed["wait_energy"];
    m_till_energy = parsed["till_energy"];
    m_energy_per_child = parsed["energy_per_child"];
    m_energy_for_photosyntesis = parsed["energy_for_photosyntesis"];
    m_earth_start_energy = parsed["earth_start_energy"];
    m_actor_start_energy = parsed["actor_start_energy"];
    m_n = parsed["n"];
    m_m = parsed["m"];
    m_min_height = parsed["min_height"];
    m_max_height = parsed["max_height"];
    m_min_height_photo_zero_multiplier = parsed["min_height_photo_zero_multiplier"];
    m_max_height_photo_zero_multiplier = parsed["max_height_photo_zero_multiplier"];
    m_pick_height_photo_multiplier = parsed["pick_height_photo_multiplier"];
    m_pick_multiplier = parsed["pick_multiplier"];
    m_smoothing_iterations = parsed["smoothing_iterations"];
    m_colored_output_mode = parsed["colored_output_mode"];
    m_heights_mode = parsed["heights_mode"];
}

long double WorldConfiguration::get_energy_for_bite() const { return m_energy_for_bite; }
long double WorldConfiguration::get_mutation_probability() const { return m_mutation_probability; }
long double WorldConfiguration::get_world_occupancy() const { return m_world_occupancy; }
long double WorldConfiguration::get_corpse_energy() const { return m_corpse_energy; }
long double WorldConfiguration::get_corpse_rotting() const { return m_corpse_rotting; }
long double WorldConfiguration::get_age_penalty() const { return m_age_penalty; }
long double WorldConfiguration::get_corpse_energy_penalty() const { return m_corpse_energy_penalty; }
long double WorldConfiguration::get_wait_energy() const { return m_wait_energy; }
long double WorldConfiguration::get_till_energy() const { return m_till_energy; }
long double WorldConfiguration::get_energy_per_child() const { return m_energy_per_child; }
long double WorldConfiguration::get_energy_for_photosyntesis() const { return m_energy_for_photosyntesis; }
long double WorldConfiguration::get_earth_start_energy() const { return m_earth_start_energy; }
long double WorldConfiguration::get_actor_start_energy() const { return m_actor_start_energy; }
size_t WorldConfiguration::get_n() const { return m_n; }
size_t WorldConfiguration::get_m() const { return m_m; }
long double WorldConfiguration::get_min_height() const { return m_min_height; }
long double WorldConfiguration::get_max_height() const { return m_max_height; }
long double WorldConfiguration::get_min_height_photo_zero_multiplier() const
{
    return m_min_height_photo_zero_multiplier;
}
long double WorldConfiguration::get_max_height_photo_zero_multiplier() const
{
    return m_max_height_photo_zero_multiplier;
}
long double WorldConfiguration::get_pick_height_photo_multiplier() const { return m_pick_height_photo_multiplier; }
long double WorldConfiguration::get_pick_multiplier() const { return m_pick_multiplier; }

size_t WorldConfiguration::get_smoothing_iterations() const { return m_smoothing_iterations; }
std::string WorldConfiguration::get_colored_output_mode() const { return m_colored_output_mode; }
std::string WorldConfiguration::get_heights_mode() const { return m_heights_mode; }
