#include "operation.hpp"

#include <algorithm>
#include <set>

Operation::Operation(const OperationType& _type) : type_(_type) {}

bool Operation::IsWorldOperation()
{
    const static std::set<OperationType> worldOperations{OperationType::PHOTOSYNTESIS,
                                                         OperationType::MAKE_CHILD,
                                                         OperationType::DIE,
                                                         OperationType::GO,
                                                         OperationType::TILL,
                                                         OperationType::WAIT,
                                                         OperationType::CHEMICAL_REACTION};
    return std::find(worldOperations.begin(), worldOperations.end(), type_) != worldOperations.end();
}

bool operator==(const Operation& lhs, const Operation& rhs)
{
    return lhs.type_ == rhs.type_ && lhs.direction_ == rhs.direction_;
}

bool operator!=(const Operation& lhs, const Operation& rhs) { return !(lhs == rhs); }

std::string Operation::ToString() const
{
    if (OperationTypeToString.find(type_) == OperationTypeToString.end()) {
        return std::string("Unknown operation, OperationType = ")
            .append(std::to_string(static_cast<uint8_t>(type_)))
            .append(".");
    }
    std::string humanReadable = OperationTypeToString.at(type_);
    if (direction_.has_value()) {
        humanReadable.append(" ").append(directionsToString.at(direction_.value()));
    }
    return humanReadable;
}

bool IsLogicalOperator(const LogicalOperator& input)
{
    static const std::set<LogicalOperator> LogicalOperators{LogicalOperator::BOOL_CONST,
                                                            LogicalOperator::LESS,
                                                            LogicalOperator::MORE,
                                                            LogicalOperator::EQUAL_BOOLS,
                                                            LogicalOperator::EQUAL_NUMERIC,
                                                            LogicalOperator::EQUAL_SENSORS,
                                                            LogicalOperator::NO_EQUAL_BOOLS,
                                                            LogicalOperator::NO_EQUAL_NUMERIC,
                                                            LogicalOperator::NO_EQUAL_SENSORS,
                                                            LogicalOperator::NO,
                                                            LogicalOperator::LESS_EQUAL,
                                                            LogicalOperator::MORE_EQUAL,
                                                            LogicalOperator::AND,
                                                            LogicalOperator::OR};
    return (LogicalOperators.find(input) != LogicalOperators.end());
}
bool IsReceivingTwoNumerics(const MathOperator& input)
{
    static const std::set<MathOperator> BinaryNumericBools{MathOperator::PLUS, MathOperator::MINUS,
                                                           MathOperator::REST_DIVIDE, MathOperator::MULTIPLE,
                                                           MathOperator::DIVIDE};
    return (BinaryNumericBools.find(input) != BinaryNumericBools.end());
}

bool IsReceivingTwoNumerics(const LogicalOperator& input)
{
    static const std::set<LogicalOperator> BinaryNumericBools{
        LogicalOperator::LESS,          LogicalOperator::MORE,
        LogicalOperator::EQUAL_NUMERIC, LogicalOperator::NO_EQUAL_NUMERIC,
        LogicalOperator::LESS_EQUAL,    LogicalOperator::MORE_EQUAL};
    return (BinaryNumericBools.find(input) != BinaryNumericBools.end());
}

bool IsReceivingOneBool(const LogicalOperator& input)
{
    static const std::set<LogicalOperator> BoolUnaryOperations{LogicalOperator::NO};
    return (BoolUnaryOperations.find(input) != BoolUnaryOperations.end());
}

bool IsReceivingTwoBools(const LogicalOperator& input)
{
    static const std::set<LogicalOperator> BinaryLogicalBools{
        LogicalOperator::EQUAL_BOOLS, LogicalOperator::NO_EQUAL_BOOLS, LogicalOperator::AND, LogicalOperator::OR};
    return (BinaryLogicalBools.find(input) != BinaryLogicalBools.end());
}

bool IsReceivingTwoSensors(const LogicalOperator& input)
{
    static const std::set<LogicalOperator> BinarySensoringBools{LogicalOperator::EQUAL_SENSORS,
                                                                LogicalOperator::NO_EQUAL_SENSORS};
    return (BinarySensoringBools.find(input) != BinarySensoringBools.end());
}