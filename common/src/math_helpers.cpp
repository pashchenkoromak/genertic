#include "math_helpers.hpp"

#include <math.h>

#include <random>

std::knuth_b rand_engine; // replace knuth_b with one of the engines listed below

// https://stackoverflow.com/a/20309151
bool rand_bool(double prob) // probability between 0.0 and 1.0
{
    std::bernoulli_distribution d(prob);
    return d(rand_engine);
}

bool double_eq(double a, double b, const double eps /* = EPS*/) { return abs(a - b) < EPS; }

/**
 * Get random value [0, max)
 */
long long getRandValue(const long long max) { return rand() % max; }

Point getNewPos(Point pos, Point direction, const Point& field_size)
{
    Point newPos;
    newPos.first = (pos.first + direction.first + field_size.first) % field_size.first;
    newPos.second = (pos.second + direction.second + field_size.second) % field_size.second;
    return newPos;
}
