rm -rf ./bin
mkdir -p bin && cd bin

conan install .. -pr ../profile

cmake ..
cmake --build . -- -j `nproc`