#pragma once
#include <iostream>

class IColorfulOutput
{
public:
    virtual void printColored(std::ostream& stream, const double height, const char symbol) const = 0;
    virtual void printColored(std::ostream& stream, const double height, const std::string& word) const = 0;
};
