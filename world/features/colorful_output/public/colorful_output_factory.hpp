#pragma once

#include <memory>

#include "icolorful_output.hpp"

class ColorfulOutputFactory
{
public:
    static std::unique_ptr<IColorfulOutput> GetColorfulOutputter(const std::string& outputType);
};
