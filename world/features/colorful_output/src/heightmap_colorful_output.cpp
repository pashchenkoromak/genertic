#include "heightmap_colorful_output.hpp"

#include <termcolor/termcolor.hpp>

void HeightmapColorfulOutput::printColored(std::ostream& stream, const double height, const char symbol) const
{
    setBackgroundColor(stream, height);
    setTextColor(stream, height);
    stream << symbol << termcolor::reset;
}

void HeightmapColorfulOutput::printColored(std::ostream& stream, const double height, const std::string& word) const
{
    setBackgroundColor(stream, height);
    setTextColor(stream, height);
    stream << word << termcolor::reset;
}

void HeightmapColorfulOutput::setBackgroundColor(std::ostream& stream, const double height) const
{
    if (height < MAX_HEIGHT_FOR_BLACK) {
        stream << termcolor::on_color<0, 0, 0>;
    } else if (height < MAX_HEIGHT_FOR_PURPLE) {
        stream << termcolor::on_color<84, 22, 180>;
    } else if (height < MAX_HEIGHT_FOR_DARK_BLUE) {
        stream << termcolor::on_color<0, 0, 139>;
    } else if (height < MAX_HEIGHT_FOR_BLUE) {
        stream << termcolor::on_color<0, 0, 255>;
    } else if (height < MAX_HEIGHT_FOR_GREY) {
        stream << termcolor::on_color<128, 128, 128>;
    } else if (height < MAX_HEIGHT_FOR_LIGHT_GREEN) {
        stream << termcolor::on_color<138, 154, 91>;
    } else if (height < MAX_HEIGHT_FOR_GREEN) {
        stream << termcolor::on_color<175, 225, 175>;
    } else if (height < MAX_HEIGHT_FOR_BROWN) {
        stream << termcolor::on_color<218, 160, 109>;
    } else if (height < MAX_HEIGHT_FOR_YELLOW) {
        stream << termcolor::on_color<255, 192, 0>;
    } else if (height < MAX_HEIGHT_FOR_RED) {
        stream << termcolor::on_color<255, 0, 0>;
    } else if (height < MAX_HEIGHT_FOR_PINK) {
        stream << termcolor::on_color<248, 131, 121>;
    } else {
        stream << termcolor::on_color<255, 255, 255>;
    }
}

void HeightmapColorfulOutput::setTextColor(std::ostream& stream, const double height) const
{
    if (height < MAX_HEIGHT_FOR_BLACK) {
        stream << termcolor::color<255, 255, 255>;
    } else if (height < MAX_HEIGHT_FOR_PURPLE) {
        stream << termcolor::color<255, 255, 255>;
    } else if (height < MAX_HEIGHT_FOR_DARK_BLUE) {
        stream << termcolor::color<255, 255, 255>;
    } else if (height < MAX_HEIGHT_FOR_BLUE) {
        stream << termcolor::color<0, 0, 0>;
    } else if (height < MAX_HEIGHT_FOR_GREY) {
        stream << termcolor::color<0, 0, 0>;
    } else if (height < MAX_HEIGHT_FOR_LIGHT_GREEN) {
        stream << termcolor::color<0, 0, 0>;
    } else if (height < MAX_HEIGHT_FOR_GREEN) {
        stream << termcolor::color<0, 0, 0>;
    } else if (height < MAX_HEIGHT_FOR_BROWN) {
        stream << termcolor::color<0, 0, 0>;
    } else if (height < MAX_HEIGHT_FOR_YELLOW) {
        stream << termcolor::color<0, 0, 0>;
    } else if (height < MAX_HEIGHT_FOR_RED) {
        stream << termcolor::color<0, 0, 0>;
    } else if (height < MAX_HEIGHT_FOR_PINK) {
        stream << termcolor::color<0, 0, 0>;
    } else {
        stream << termcolor::color<0, 0, 0>;
    }
}