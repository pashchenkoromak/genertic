#include "colorful_output_factory.hpp"

#include <memory>

#include "heightmap_colorful_output.hpp"
#include "mock_colorful_output.hpp"

std::unique_ptr<IColorfulOutput> ColorfulOutputFactory::GetColorfulOutputter(const std::string& outputType)
{
    if (outputType == "heightmap") return std::move(std::make_unique<HeightmapColorfulOutput>());
    // if (outputType == "none")
    else
        return std::move(std::make_unique<MockColorfulOutput>());
}
