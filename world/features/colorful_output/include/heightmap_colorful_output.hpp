#pragma once
#include <iostream>

#include "icolorful_output.hpp"

class HeightmapColorfulOutput : public IColorfulOutput
{
public:
    virtual void printColored(std::ostream& stream, const double height, const char symbol) const override;
    virtual void printColored(std::ostream& stream, const double height, const std::string& word) const override;

private:
    const int MAX_HEIGHT_FOR_BLACK = 50;
    const int MAX_HEIGHT_FOR_PURPLE = 150;
    const int MAX_HEIGHT_FOR_DARK_BLUE = 300;
    const int MAX_HEIGHT_FOR_BLUE = 500;
    const int MAX_HEIGHT_FOR_GREY = 800;
    const int MAX_HEIGHT_FOR_LIGHT_GREEN = 1100;
    const int MAX_HEIGHT_FOR_GREEN = 1400;
    const int MAX_HEIGHT_FOR_BROWN = 1700;
    const int MAX_HEIGHT_FOR_YELLOW = 2200;
    const int MAX_HEIGHT_FOR_RED = 2500;
    const int MAX_HEIGHT_FOR_PINK = 2700;
    const int MAX_HEIGHT_FOR_WHITE = 3000;
    void setBackgroundColor(std::ostream& stream, const double height) const;
    void setTextColor(std::ostream& stream, const double height) const;
};
