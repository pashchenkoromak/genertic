#pragma once
#include <iostream>

#include "icolorful_output.hpp"

// No logic depending on height
class MockColorfulOutput : public IColorfulOutput
{
public:
    virtual void printColored(std::ostream& stream, const double height, const char symbol) const { stream << symbol; }
    virtual void printColored(std::ostream& stream, const double height, const std::string& word) const
    {
        stream << word;
    }
};
