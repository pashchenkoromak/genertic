#include <memory>

#include "iheights_manager.hpp"

class HeightsManagerFactory
{
public:
    static std::unique_ptr<IHeightsManager> GetHeightsManager(const std::string& heightsMapMode);
};
