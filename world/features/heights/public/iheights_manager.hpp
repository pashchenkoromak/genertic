#pragma once
#include <utility>

#include "worldConfiguration.hpp"

typedef std::pair<int, int> Point;

class IHeightsManager
{
public:
    virtual void Init(const WorldConfiguration& config) = 0;
    virtual double GetHeight(const Point& pos) const = 0;
    virtual double GetEnergyMultiplayer(const Point& pos) const = 0;
    virtual void OnTill(const Point& pos, const double energy) = 0;
    virtual void OnDeath(const Point& pos, const double energy) = 0;
    virtual void OnIterationEnd() = 0;
    virtual double GetChemicalReactionEnergy(const Point& pos) = 0;
};
