#include <vector>

#include "iheights_manager.hpp"

class SimpleHeightsManager : public IHeightsManager
{
public:
    virtual void Init(const WorldConfiguration& config) override;
    virtual double GetHeight(const Point&) const override;
    virtual double GetEnergyMultiplayer(const Point& pos) const override;
    virtual void OnTill(const Point& pos, const double energy) override{};
    virtual void OnDeath(const Point& pos, const double energy) override{};
    virtual void OnIterationEnd() override{};
    virtual double GetChemicalReactionEnergy(const Point& pos) override { return 0; };

private:
    void GenerateHeights(const WorldConfiguration& config);
    void GenerateRandomHeights(const int min_height, const int max_height);
    void PresetHeights();
    void ApplyHeights();
    void fillHeights();

    double GetAverageHeightsAroundPoint(const Point center) const;

    Point world_size_;
    std::vector<std::vector<double>> heights_;
    // needed for generation
    std::vector<std::vector<double>> preheights_;
};
