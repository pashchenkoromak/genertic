#include "iheights_manager.hpp"

class CraftHeightsManager : public IHeightsManager
{
public:
    virtual void Init(const WorldConfiguration& config) override;
    virtual double GetHeight(const Point&) const override;
    virtual double GetEnergyMultiplayer(const Point& pos) const override;
    virtual void OnTill(const Point& pos, const double energy) override;
    virtual void OnDeath(const Point& pos, const double energy) override;
    virtual void OnIterationEnd() override;
    virtual double GetChemicalReactionEnergy(const Point& pos) override;

private:
    /// @brief slide land from the point to points nearby
    /// @param pos
    void LandSlide(const Point& pos);
    Point world_size_;

    /// @brief on height close to "pickPhotosyntesis" height-multiplier will be the biggest
    double pick_photosyntesis_;
    /// @brief This is maximal possible coefficient. Lineary decreases from pick to both sides.
    double pick_multiplier_;
    /// @brief multiplier for min and max will be zero on these moments. For heights <min
    /// and >max height-multiplier is negative.
    double min_zero_photosyntesis, max_zero_photosyntesis;

    std::vector<std::vector<double>> heights_;
};
