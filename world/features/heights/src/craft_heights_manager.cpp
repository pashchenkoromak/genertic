#include "craft_heights_manager.hpp"

#include <cmath>
#include <cstddef>

#include "math_helpers.hpp"
#include "operation.hpp"

void CraftHeightsManager::Init(const WorldConfiguration& config)
{
    world_size_ = std::make_pair(config.get_n(), config.get_m());
    heights_.resize(world_size_.first, std::vector<double>(world_size_.second, 1000));
    pick_multiplier_ = config.get_pick_multiplier();
    max_zero_photosyntesis = config.get_max_height_photo_zero_multiplier();
    min_zero_photosyntesis = config.get_min_height_photo_zero_multiplier();
    pick_photosyntesis_ = config.get_pick_height_photo_multiplier();
}

double CraftHeightsManager::GetHeight(const Point& pos) const { return heights_[pos.first][pos.second]; }

double CraftHeightsManager::GetEnergyMultiplayer(const Point& pos) const
{
    double h = heights_[pos.first][pos.second];
    double coef;
    if (h <= pick_photosyntesis_) {
        coef = (h - min_zero_photosyntesis) / (pick_photosyntesis_ - min_zero_photosyntesis);
    } else {
        coef = (h - max_zero_photosyntesis) / (pick_photosyntesis_ - max_zero_photosyntesis);
    }
    return coef * pick_multiplier_;
}

void CraftHeightsManager::OnTill(const Point& pos, const double energy)
{
    heights_[pos.first][pos.second] -= (energy / 10);
}

double CraftHeightsManager::GetChemicalReactionEnergy(const Point& pos)
{
    double h = heights_[pos.first][pos.second];
    if (h > 1000) {
        return 0;
    }
    return (1000 - h);
}

void CraftHeightsManager::OnDeath(const Point& pos, const double energy)
{
    heights_[pos.first][pos.second] += (energy / 10);
}

void CraftHeightsManager::OnIterationEnd()
{
    for (size_t x = 0; x < world_size_.first; x++) {
        for (size_t y = 0; y < world_size_.second; y++) {
            double height = heights_[x][y];
            Point pos = {x, y};
            for (const auto& [direction, point] : directionsToPoint) {
                Point otherPos = getNewPos(pos, point, world_size_);
                double diff = height - heights_[otherPos.first][otherPos.second];
                if (diff > 100) {
                    heights_[x][y] -= (diff / 100);
                    heights_[otherPos.first][otherPos.second] += (diff / 100);
                }
            }
        }
    }
}
