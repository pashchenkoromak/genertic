#include "mock_heights_manager.hpp"

#include <cstddef>

void MockHeightsManager::Init(const WorldConfiguration& config) { return; }
double MockHeightsManager::GetHeight(const Point&) const { return 1; }
double MockHeightsManager::GetEnergyMultiplayer(const Point& pos) const { return 1; }
void MockHeightsManager::OnTill(const Point& pos, const double energy) {}
void MockHeightsManager::OnDeath(const Point& pos, const double energy) {}
void MockHeightsManager::OnIterationEnd() {}
double MockHeightsManager::GetChemicalReactionEnergy(const Point& pos) { return 0; }