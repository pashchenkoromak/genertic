#include "heights_manager_factory.hpp"

#include <memory>

#include "craft_heights_manager.hpp"
#include "mock_heights_manager.hpp"
#include "simple_heights_manager.hpp"

std::unique_ptr<IHeightsManager> HeightsManagerFactory::GetHeightsManager(const std::string& heightsMode)
{
    if (heightsMode == "simple") {
        return std::move(std::make_unique<SimpleHeightsManager>());
    } else if (heightsMode == "craft") {
        return std::move(std::make_unique<CraftHeightsManager>());
    } else {
        return std::move(std::make_unique<MockHeightsManager>());
    }
}
