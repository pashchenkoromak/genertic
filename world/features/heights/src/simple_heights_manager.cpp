#include "simple_heights_manager.hpp"

#include <cstddef>

#include "math_helpers.hpp"
#include "operation.hpp"

void SimpleHeightsManager::Init(const WorldConfiguration& config)
{
    world_size_ = std::make_pair(config.get_n(), config.get_m());
    heights_.resize(world_size_.first, std::vector<double>(world_size_.second));
    preheights_.resize(world_size_.first, std::vector<double>(world_size_.second));
    GenerateHeights(config);
}

double SimpleHeightsManager::GetHeight(const Point& pos) const { return heights_[pos.first][pos.second]; }

double SimpleHeightsManager::GetEnergyMultiplayer(const Point& pos) const
{
    return heights_[pos.first][pos.second] / 100;
}

void SimpleHeightsManager::GenerateHeights(const WorldConfiguration& config)
{
    GenerateRandomHeights(config.get_min_height(), config.get_max_height());
    size_t iterations = config.get_smoothing_iterations();
    do {
        PresetHeights();
        ApplyHeights();
    } while (iterations--);
}

void SimpleHeightsManager::GenerateRandomHeights(const int min_height, const int max_height)
{
    for (auto& row : heights_) {
        for (auto& height : row) {
            height = getRandValue(max_height - min_height) + min_height;
        }
    }
}

double SimpleHeightsManager::GetAverageHeightsAroundPoint(const Point center) const
{
    double sumHeight = 0;
    size_t amountHeights = 0;
    for (const auto& [direction, point] : directionsToPoint) {
        Point otherPos = getNewPos(center, point, world_size_);
        sumHeight += heights_[otherPos.first][otherPos.second];
        amountHeights++;
    }
    return sumHeight / amountHeights;
}

void SimpleHeightsManager::PresetHeights()
{
    for (size_t i = 0; i < world_size_.first; i++) {
        for (size_t j = 0; j < world_size_.second; j++) {
            preheights_[i][j] = GetAverageHeightsAroundPoint(std::make_pair(i, j));
        }
    }
}

void SimpleHeightsManager::ApplyHeights()
{
    for (size_t i = 0; i < world_size_.first; i++) {
        for (size_t j = 0; j < world_size_.second; j++) {
            heights_[i][j] = preheights_[i][j];
        }
    }
}
