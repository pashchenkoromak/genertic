cmake_minimum_required(VERSION 2.8.12)

project(heights_lib)

set(PUBLIC_HEADERS public/heights_manager_factory.hpp public/iheights_manager.hpp)
set(INCLUDE_HEADERS include/mock_heights_manager.hpp include/simple_heights_manager.hpp include/craft_heights_manager.hpp)
set(SOURCES src/heights_manager_factory.cpp src/simple_heights_manager.cpp src/mock_heights_manager.cpp src/craft_heights_manager.cpp)

add_library(${PROJECT_NAME} ${PUBLIC_HEADERS} ${INCLUDE_HEADERS} ${SOURCES})
target_include_directories(${PROJECT_NAME} INTERFACE public/)
target_include_directories(${PROJECT_NAME} PRIVATE ${CONAN_INCLUDE_DIRS_TERMCOLOR} include/ public/) 
target_link_libraries(${PROJECT_NAME} common_lib)

#if (${COVERAGE_ENABLE}) 
#    enable_coverage(${PROJECT_NAME}) 
#endif()

#add_subdirectory(test)
