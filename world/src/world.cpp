#include "world.hpp"

#include <algorithm>
#include <iostream>
#include <iterator>
#include <random>
#include <sstream>
#include <string>

#include "colorful_output_factory.hpp"
#include "heights_manager_factory.hpp"
#include "math_helpers.hpp"
#include "operation.hpp"

using std::make_pair;

World::World(const WorldConfiguration& config_)
    : config_(config_), world_size_(std::make_pair(config_.get_n(), config_.get_m())), turns_to_shift_(10)
{
    colorful_output_ = std::move(ColorfulOutputFactory::GetColorfulOutputter(config_.get_colored_output_mode()));
    heights_manager_ = std::move(HeightsManagerFactory::GetHeightsManager(config_.get_heights_mode()));
    heights_manager_->Init(config_);

    world_.resize(config_.get_n());
    for (int i = 0; i < config_.get_n(); i++) {
        world_[i].resize(config_.get_m());
        for (int j = 0; j < config_.get_m(); j++) {
            world_[i][j].energy_ = getRandValue(1000);
            world_[i][j].pos_ = make_pair(i, j);
            if (rand_bool(config_.get_world_occupancy())) {
                world_[i][j].AddActor(std::make_shared<Actor>(getRandValue(config_.get_actor_start_energy())));
            }
        }
    }
}

void World::FillActions()
{
    for (auto& row : world_) {
        for (auto& worldPoint : row) {
            for (auto& actor : worldPoint.actors_) {
                if (!actor->IsCorpse()) {
                    SetTouchSensors(make_pair(worldPoint.pos_, actor));
                    actor->prepareNextMove();
                } else {
                    heights_manager_->OnDeath(worldPoint.pos_, actor->getEnergy());
                    worldPoint.RemoveActor(actor);
                }
            }
        }
    }
}
void World::ApplyActions()
{
    for (auto& row : world_) {
        for (auto& worldPoint : row) {
            for (auto& actor : worldPoint.actors_) {
                if (!actor->IsCorpse()) {
                    HandleOperation(actor->GetNextMove(), make_pair(worldPoint.pos_, actor));
                } else {
                    actor->changeEnergy(
                        static_cast<long long>(-config_.get_corpse_energy() * config_.get_corpse_rotting()));
                }
            }
        }
    }
}

/*
a = [1,2,4,3]
1: 1/1 * 1/3 = 1/3 * 3/7 = 1/7 * 7/10 = 1/10
2: 2/3 = 2/3 * 3/7 = 2/7 * 7/10 = 2/10
3: 4/7 = 4/7 * 7/10 = 4/10
4: 3/10
*/

void World::ResolveCollisions()
{
    for (auto& row : world_) {
        for (auto& worldPoint : row) {
            worldPoint.RemoveCorpses();
            long long sumEnergy = 0;
            std::shared_ptr<Actor> chosen;
            for (auto& actor : worldPoint.actors_) {
                long long actorEnergy = actor->getEnergy();
                sumEnergy += actorEnergy;
                if (rand_bool((actorEnergy + .0) / (sumEnergy + 0.0))) {
                    chosen = actor;
                }
            }
            worldPoint.KillAllExcept(chosen);
        }
    }
}

void World::GetNextMove()
{
    FillActions();
    ApplyActions();
    ResolveCollisions();
    if (turns_to_shift_ == 0) {
        turns_to_shift_ = 10;
        heights_manager_->OnIterationEnd();
    } else {
        turns_to_shift_--;
    }
}

void World::ApplyAgePenalty(std::shared_ptr<Actor> actor)
{
    long long genomLength = static_cast<long long>(actor->getGenomLength());
    actor->changeEnergy(-genomLength / 50);
}

void World::HandleOperation(const Operation& operation, PosActor actor)
{
    ApplyAgePenalty(actor.second);
    switch (operation.type_) {
        case OperationType::GO:
            Go(actor, operation.direction_.value());
            break;
        case OperationType::WAIT:
            Wait(actor);
            break;
        case OperationType::DIE:
            Die(actor);
            break;
        case OperationType::MAKE_CHILD:
            MakeChild(actor);
            break;
        case OperationType::PHOTOSYNTESIS:
            Photosynthesis(actor);
            break;
        case OperationType::TILL:
            Till(actor);
            break;
        case OperationType::CHEMICAL_REACTION:
            ChemicalReaction(actor);
            break;
        default:
            Wait(actor);
    }
}
void World::Go(PosActor actor, eDirection direction)
{
    Point pos = actor.first;
    auto newPos = getNewPos(pos, directionsToPoint.at(direction), world_size_);
    world_[newPos.first][newPos.second].AddActor(actor.second);
    world_[pos.first][pos.second].RemoveActor(actor.second);
}

void World::Wait(PosActor actor) { actor.second->changeEnergy(-config_.get_wait_energy()); }

void World::Die(PosActor actor)
{
    //    std::cout << "actor at pos=(" << actor.first.first << ',' << actor.first.second << ") and id_=" <<
    //    actor.second->getId() << " wants to die\n";
    heights_manager_->OnDeath(actor.first, actor.second->getEnergy());
    world_[actor.first.first][actor.first.second].RemoveActor(actor.second);
}

void World::Till(PosActor actor)
{
    Point pos = actor.first;
    long double energyChange = world_[pos.first][pos.second].energy_ * config_.get_till_energy();
    actor.second->changeEnergy(energyChange);
    world_[pos.first][pos.second].energy_ -= energyChange;
    heights_manager_->OnTill(pos, energyChange);
}

void World::ChemicalReaction(PosActor actor)
{
    Point pos = actor.first;
    long double energyChange = heights_manager_->GetChemicalReactionEnergy(pos);
    actor.second->changeEnergy(energyChange);
    for (int i = 0; i < energyChange / 100; i++) {
        actor.second->mutation();
    }
}

void World::MakeChild(PosActor actor)
{
    Point pos = actor.first;
    for (const auto& [direction, dirPoint] : directionsToPoint) {
        long long energyPerChild = actor.second->getEnergy() * config_.get_energy_per_child();
        actor.second->changeEnergy(-energyPerChild);
        Point newPos = getNewPos(pos, dirPoint, world_size_);
        std::shared_ptr<Actor> child = Actor::Child(*actor.second, energyPerChild);
        if (rand_bool(config_.get_mutation_probability())) {
            child->mutation();
        }
        world_[newPos.first][newPos.second].AddActor(child);
    }
}

void World::Photosynthesis(PosActor actor)
{
    actor.second->changeEnergy(config_.get_energy_for_photosyntesis() *
                               heights_manager_->GetEnergyMultiplayer(actor.first));
}

size_t World::GetNumberOfAlives() const
{
    size_t result = 0;
    for (const auto& row : world_) {
        for (const auto& worldPoint : row) {
            for (const auto& actor : worldPoint.actors_)
                if (!actor->IsCorpse()) {
                    result++;
                }
        }
    }
    return result;
}

void World::Show() const
{
    for (const auto& row : world_) {
        for (const auto& worldPoint : row) {
            if (worldPoint.Empty()) {
                colorful_output_->printColored(std::cout, heights_manager_->GetHeight(worldPoint.pos_), '_');
                continue;
            }
            const auto& actor = worldPoint.GetFirstActor();

            if (actor && actor->IsCorpse()) {
                colorful_output_->printColored(std::cout, heights_manager_->GetHeight(worldPoint.pos_), 'x');
            } else {
                colorful_output_->printColored(std::cout, heights_manager_->GetHeight(worldPoint.pos_), 'O');
            }
        }
        std::cout << std::endl;
    }
    PrintStats();
}

void World::PrintMind() const
{
    long long count = 0;
    for (const auto& row : world_) {
        for (const auto& worldPoint : row) {
            for (const auto& actor : worldPoint.actors_) {
                if (!actor->IsCorpse()) {
                    count++;
                    if (count % 10 == 1) {
                        std::cout << "Minds of actor " << actor->getId() << " with num " << count << std::endl;
                        std::cout << "Genom length " << actor->getGenomLength() << std::endl;
                        std::cout << "Actor energy " << actor->getEnergy() << std::endl;
                        std::cout << actor->getMinds() << std::endl << std::endl;
                    }
                }
            }
        }
    }
}

void World::SetTouchSensors(PosActor actor)
{
    std::vector<touchSensorStatus> sensors(8);
    for (std::pair<eDirection, std::pair<int, int>> direction : directionsToPoint) {
        const auto& newPos = getNewPos(actor.first, direction.second, world_size_);
        const size_t sensorId = static_cast<size_t>(direction.first);
        auto& actorsOnPoint = world_[newPos.first][newPos.second].actors_;
        if (world_[newPos.first][newPos.second].Empty())
            sensors[sensorId] = touchSensorStatus::Empty;
        else if (actorsOnPoint.size() > 1)
            sensors[sensorId] = touchSensorStatus::Mess;
        else if (actorsOnPoint.begin()->get()->IsCorpse())
            sensors[sensorId] = touchSensorStatus::Corpse;
        else
            sensors[sensorId] = touchSensorStatus::Actor;
    }
}
void World::PrintStats() const
{
    double sumEnergy = 0;
    size_t actorAmount = 0;
    size_t sumAge = 0;
    for (const auto& row : world_) {
        for (const auto& worldPoint : row) {
            for (const auto& actor : worldPoint.actors_) {
                if (!actor->IsCorpse()) {
                    sumEnergy += actor->getEnergy();
                    actorAmount++;
                    sumAge += actor->getAge();
                }
            }
        }
    }
    std::cout << "Summary energy : " << sumEnergy << std::endl
              << "Average age : " << (sumAge + .0) / actorAmount << std::endl;
}
