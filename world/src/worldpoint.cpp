#include "worldpoint.hpp"

WorldPoint::WorldPoint() : energy_(0), pos_(0, 0) {}

WorldPoint::WorldPoint(Point _pos) : energy_(0), pos_(_pos) {}

void WorldPoint::AddActor(std::shared_ptr<Actor> actor) { actors_.insert(actor); }

void WorldPoint::RemoveActor(std::shared_ptr<Actor> removing_actor)
{
    for (auto& actor : actors_) {
        if (actor == removing_actor) {
            actor->die();
            return;
        }
    }
}

void WorldPoint::RemoveCorpses()
{
    for (auto it = actors_.cbegin(); it != actors_.cend();) {
        if (it->get()->IsCorpse()) {
            long long actorEnergy = it->get()->getEnergy();
            energy_ += actorEnergy;
            it = actors_.erase(it);
        } else {
            ++it;
        }
    }
}

void WorldPoint::KillAllExcept(std::shared_ptr<Actor> remaining)
{
    for (auto& actor : actors_) {
        if (actor != remaining) {
            actor->die();
        }
    }
}

bool WorldPoint::Empty() const { return actors_.empty(); }

/// @brief  NO CHECKS!!!!!
/// @param index
/// @return
std::shared_ptr<Actor> WorldPoint::GetFirstActor() const { return *(actors_.begin()); }
void WorldPoint::SetAction(std::shared_ptr<Actor> actor, Operation action)
{
    auto m_actor = actors_.find(actor);
    if (m_actor != actors_.end()) {
        m_actor->get()->SetNextMove(action);
    }
}

Operation WorldPoint::GetAction(std::shared_ptr<Actor> actor) const
{
    auto m_actor = actors_.find(actor);
    if (m_actor != actors_.end()) return m_actor->get()->GetNextMove();
    return Operation(OperationType::DIE);
}
