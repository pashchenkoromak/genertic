#ifndef WORLD_HPP
#define WORLD_HPP

#include <memory>
#include <set>
#include <vector>

#include "actor.hpp"
#include "icolorful_output.hpp"
#include "iheights_manager.hpp"
#include "math_helpers.hpp"
#include "worldConfiguration.hpp"
#include "worldpoint.hpp"

/// @class World
/// @brief it includes actors and can wrap their moves.
class World
{
public:
    /// @brief Constructor. It creates a world, and fill it due to FULLNESS with
    /// actors.
    World(const WorldConfiguration&);

    /// @brief Default destructor
    ~World() = default;

    /// @brief Get next state of the world.
    void GetNextMove();

    /// @brief Get number of Alives actor in the world
    size_t GetNumberOfAlives() const;

    /// @brief show the world to the std::cout
    void Show() const;

    void PrintMind() const;

private:
    void SetTouchSensors(PosActor actor);

    const WorldConfiguration config_;

    /// @brief Some position, that means "HERE IS NO POSITION!".
    const Point NULL_POS = std::make_pair(SIZE_MAX, SIZE_MAX);

    /// @brief Ask every actor what he wanna do
    void FillActions();
    /// @brief Apply actions of every actor
    void ApplyActions();
    /// @brief Resolve cases when there are >1 actors in one pos
    void ResolveCollisions();
    /// @brief Make actors pay for their age
    /// @param actor
    void ApplyAgePenalty(std::shared_ptr<Actor> actor);
    /// @brief Print some statistical data for the whole world
    void PrintStats() const;

    /// @brief Actors matrix
    std::vector<std::vector<WorldPoint>> world_;

    /// @brief Handles operation by actor
    void HandleOperation(const Operation& operation, PosActor actor);

    /// @brief Add energy to the actor, due to his position
    /// @param[in] actor - doer
    void Photosynthesis(PosActor actor);

    /// @brief move actor to the direction
    /// @param[in] actor
    /// @param[in] direction
    void Go(PosActor actor, eDirection direction);

    /// @brief actor doesnt want to do anything
    /// @param[in] actor - some lazy actor
    void Wait(PosActor actor);

    /// @brief create a corpse from a actor
    /// @param[in] actor - actor, who will die
    void Die(PosActor actor);

    /// @brief till ground under the actor to get energy.
    /// @param[in] actor - tiller
    void Till(PosActor actor);

    /// @brief Does a chemical reaction with earth. It produces a lot of energy but make unit mutate.
    /// @param actor - position and than - actor
    void ChemicalReaction(PosActor actor);

    /// @brief creates children where it's possible around the actor
    /// @param[in] actor - parent
    void MakeChild(PosActor actor);

    std::unique_ptr<IColorfulOutput> colorful_output_;
    std::unique_ptr<IHeightsManager> heights_manager_;
    const Point world_size_;
    int turns_to_shift_;
};

#endif // WORLD_HPP