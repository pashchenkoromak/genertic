#ifndef WORLDPOINT_HPP
#define WORLDPOINT_HPP

#include <map>
#include <memory>

#include "actor.hpp"
#include "math_helpers.hpp"

typedef std::pair<Point, std::shared_ptr<Actor>> PosActor;

struct WorldPoint
{
    WorldPoint();
    WorldPoint(Point _pos);
    void AddActor(std::shared_ptr<Actor> actor);
    void RemoveActor(std::shared_ptr<Actor> removing_actor);
    void RemoveCorpses();
    void KillAllExcept(std::shared_ptr<Actor> remaining);

    /// @brief Check if there are actors on the point.
    /// @return true if it's empty, false if full.
    bool Empty() const;

    /// @brief  NO CHECKS!!!!!
    /// @param index
    /// @return
    std::shared_ptr<Actor> GetFirstActor() const;
    void SetAction(std::shared_ptr<Actor> actor, Operation action);

    Operation GetAction(std::shared_ptr<Actor> actor) const;

    long long energy_;
    std::set<std::shared_ptr<Actor>> actors_;
    Point pos_;
};

#endif // WORLDPOINT_HPP