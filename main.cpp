#include <fstream>
#include <iostream>
#include <mutex>
#include <shared_mutex>
#include <thread>

#include "world.hpp"
#include "worldConfiguration.hpp"

bool stopflag = false;
std::shared_mutex stopflag_mutex;

bool pauseflag = false;
std::shared_mutex pauseflag_mutex;

bool outputflag = false;
std::shared_mutex outputflag_mutex;

bool skipflag = false;
std::shared_mutex skipflag_mutex;

bool paused()
{
    std::shared_lock pause_lock(pauseflag_mutex);
    return pauseflag;
}

void userInput()
{
    char command;
    while (std::cin >> command) {
        if (command == 'q') {
            std::unique_lock lock(stopflag_mutex);
            stopflag = true;
            return;
        }
        if (command == 'p') {
            std::unique_lock lock(pauseflag_mutex);
            pauseflag = true;
            std::cout << "To continue put c.\n";
            continue;
        }
        if (command == 'c') {
            std::unique_lock lock(pauseflag_mutex);
            pauseflag = false;
            continue;
        }
        if (command == 'o') {
            if (!paused()) {
                std::unique_lock pause_lock(pauseflag_mutex);
                pauseflag = true;
            }
            std::unique_lock o_lock(outputflag_mutex);
            outputflag = true;
            continue;
        }
        if (command == 'h') {
            std::unique_lock lock(pauseflag_mutex);
            pauseflag = true;
            std::cout << "q - to quit\np - to pause\nc - to continue\no - to output\ns - to enable and disable "
                         "skipping mod.\nh - for help"
                      << std::endl;
            continue;
            ;
        }
        if (command == 's') {
            std::unique_lock lock(skipflag_mutex);
            skipflag = !skipflag;
            continue;
            ;
        }
    }
}

void runWorld(const WorldConfiguration& config)
{
    World test(config);
    int j = 0;
    do {
        // exit
        std::shared_lock lock(stopflag_mutex);
        if (stopflag) {
            return;
        }

        // pause
        std::shared_lock p_lock(pauseflag_mutex);
        if (pauseflag) {
            // print minds
            std::unique_lock o_lock(outputflag_mutex);
            if (outputflag) {
                std::cout << "\n\n";
                test.PrintMind();
                std::cout << "\n\n";
                outputflag = false;
            }
            continue;
        }

        // skip mode
        std::shared_lock s_lock(skipflag_mutex);
        if (!skipflag) system("sleep 0.1");

        j++;
        system("clear");
        test.Show();
        std::cout << "Iteration " << j << std::endl;
        test.GetNextMove();
    } while (true);
}

int main()
{
    std::thread inputThread(userInput);
    const WorldConfiguration config(std::ifstream("config.json"));
    runWorld(config);
    inputThread.join();
    return 0;
}
