#include "actor.hpp"

Actor::Actor(const long long energy) : energy_(energy), age_(0) { id_ = Actor::nextId++; }

Actor::Actor(const Genom& rhs) : genom_(rhs), id_(nextId++), age_(0) {}

std::shared_ptr<Actor> Actor::Child(const Actor& rhs, const long long energy)
{
    std::shared_ptr<Actor> child = std::make_shared<Actor>(rhs.genom_);
    child->energy_ = energy;
    child->next_action_ = Operation(OperationType::WAIT);
    return child;
}

std::string Actor::getMinds() const { return genom_.GetGenomString().append("\n").append(genom_.getMinds()); }

void Actor::mutation() { genom_.mutation(); }

void Actor::prepareNextMove()
{
    age_++;
    double mutation_probability = 0;
    if (next_action_ == OperationType::PHOTOSYNTESIS) {
        mutation_probability = 0.05;
    }
    next_action_ = genom_.nextMove(energy_, true, mutation_probability, true);
}

void Actor::changeEnergy(const long long value) { energy_ += value; }
size_t Actor::getGenomLength() const { return genom_.size(); }

void Actor::setTouchSensorsStatuses(const std::vector<touchSensorStatus>& stats)
{
    genom_.setTouchSensorsStatuses(stats);
}

void Actor::die() { id_ = -2; }

bool Actor::IsCorpse() const { return id_ == -2; }

Actor Actor::NO_UNIT()
{
    static Actor NO_UNIT;
    NO_UNIT.id_ = -1;
    return NO_UNIT;
}

Actor Actor::CORPSE()
{
    static Actor CORPSE;
    CORPSE.id_ = -2;
    return CORPSE;
}

Operation Actor::GetNextMove() const { return next_action_; }
void Actor::SetNextMove(const Operation& nextMove) { next_action_ = nextMove; }

long long Actor::getEnergy() const { return energy_; }

long long Actor::getAge() const { return age_; }

bool operator==(const class Actor& lhs, const class Actor& rhs) { return lhs.id_ == rhs.id_; }
bool operator!=(const class Actor& lhs, const class Actor& rhs) { return !(lhs.id_ == rhs.id_); }

int Actor::nextId = 0;
