
#include "genomContainer.hpp"

#include <algorithm>
GenomContainer::GenomContainer(size_t number, Length length) : current_bit_(0)
{
    genom_ = ConvertNumToVec(number, length);
}

std::string GenomContainer::ToString() const { return VecToString(genom_); }

/// @brief Calculate the next number with length size.
/// @note Moves the beginning point
/// @param length
/// @return
size_t GenomContainer::GetNumber(Length length)
{
    if (genom_.size() == 0) {
        return 0;
    }
    size_t res = GetNumberFromPos(current_bit_, length);
    current_bit_ = (current_bit_ + length) % genom_.size();
    return res;
}

void GenomContainer::Insert(Position where, size_t number, Length length)
{
    std::vector<bool> inserting(ConvertNumToVec(number, length));
    genom_.insert(genom_.begin() + where, inserting.begin(), inserting.end());
}
void GenomContainer::Append(size_t number, Length length) { Insert(Position(genom_.size()), number, length); }

void GenomContainer::Remove(Position where, Length length)
{
    if (genom_.empty()) {
        return;
    }
    where.val = where % genom_.size();
    if (length > genom_.size()) {
        genom_.clear();
        return;
    } else if (where + length > genom_.size()) {
        size_t amountInTail = genom_.size() - where;
        genom_.erase(genom_.begin() + where, genom_.end());
        size_t amountInHead = length - amountInTail;
        genom_.erase(genom_.begin(), genom_.begin() + amountInHead);
    } else {
        genom_.erase(genom_.begin() + where, genom_.begin() + where + length);
    }
}

std::vector<bool> GenomContainer::ConvertNumToVec(size_t number, Length length)
{
    std::vector<bool> res(length);
    for (int i = length - 1; i >= 0; i--) {
        res[i] = number & 1;
        number >>= 1;
    }
    return res;
}

void GenomContainer::SetCurrentBit(Position where)
{
    if (genom_.empty()) return;
    current_bit_ = where % genom_.size();
}
size_t GenomContainer::GetCurrentBit() const { return current_bit_; }

void GenomContainer::Mutate()
{
    const size_t randomNumber = rand();
    const Position randomPos = rand() % std::max(genom_.size(), size_t(1));
    const Length randomLength = rand() % MaxMutationLength + 1;

    std::vector<size_t> probabilities{30, 90, 100};
    size_t mutationType = rand() % 100;
    if (mutationType < probabilities[0])
        genomMutation(randomNumber, randomLength);
    else if (mutationType < probabilities[1]) {
        genMutation(randomPos, randomNumber, randomLength);
    } else {
        ChromosomeMutationType type = static_cast<ChromosomeMutationType>(rand() % 3);
        chromosomeMutation(type, randomPos, randomLength);
    }
}

void GenomContainer::genomMutation(size_t number, Length length) { Insert(Position(genom_.size()), number, length); }

void GenomContainer::genMutation(Position where, size_t number, Length length)
{
    if (genom_.empty()) {
        return;
    }
    if (length > genom_.size()) {
        where = (where + length % genom_.size()) % genom_.size();
        length = genom_.size();
    }
    auto newBits = ConvertNumToVec(number, length);
    where.val = where % genom_.size();
    if (where + length > genom_.size()) {
        size_t amountInTail = genom_.size() - where;
        std::copy(newBits.begin(), newBits.begin() + amountInTail, genom_.begin() + where);

        size_t amountInHead = length - amountInTail;
        std::copy(newBits.begin() + amountInTail, newBits.begin() + length, genom_.begin());
    } else {
        std::copy(newBits.begin(), newBits.begin() + length, genom_.begin() + where);
    }
}

void GenomContainer::chromosomeMutation(ChromosomeMutationType type, Position where, Length length)
{
    switch (type) {
        case Double:
            doubleGen(where, length);
            break;
        case Erase:
            eraseGen(where, length);
            break;
        case Reverse:
            reverseGen(where, length);
            break;
    }
}

void GenomContainer::doubleGen(Position where, Length length)
{
    if (genom_.empty()) {
        return;
    }
    size_t inserting = GetNumberFromPos(where, length);
    Insert(Position((where + length) % genom_.size()), inserting, length);
}

void GenomContainer::eraseGen(Position where, Length length) { Remove(where, length); }

void GenomContainer::reverseGen(Position where, Length length)
{
    const size_t n = genom_.size();
    if (n == 0) {
        return;
    }
    for (size_t i = 0; i < length / 2; i++) {
        Position left = (where + i) % n;
        Position right = (where + length - i - 1) % n;
        std::swap(genom_[left], genom_[right]);
    }
}

std::string GenomContainer::VecToString(const std::vector<bool>& vec)
{
    if (vec.empty()) {
        return "";
    }
    size_t vecSize = vec.size();
    std::string res;
    res.reserve(vecSize);
    for (const auto& bit : vec) {
        res.append(bit ? "1" : "0");
    }
    return res;
}

size_t GenomContainer::GetNumberFromPos(Position where, Length length) const
{
    const size_t genomSize = genom_.size();
    if (genomSize == 0) {
        return 0;
    }
    size_t res = 0;
    where.val = where % genomSize;
    for (size_t i = 0; i < length; i++) {
        res = (res << 1) + (genom_[where] ? 1 : 0);
        where.val = (where + 1) % genomSize;
    }
    return res;
}

size_t GenomContainer::size() const { return genom_.size(); }
