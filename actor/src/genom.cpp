#include "genom.hpp"

#include <iostream>

#include "math_helpers.hpp"
Genom::Genom() : genom_(0, Length(0)), treshhold_energy_(0)
{
    for (; genom_.size() < Genom::DEFAULT_SIZE;) {
        size_t type = rand() % OperationTypes.size();
        genom_.Append(type, CommandLength::OPERATION);
    }
    genom_.SetCurrentBit(0);
    sensor_statuses_ = std::move(std::vector(8, touchSensorStatus::Empty));
    variables_ = std::move(std::vector((1 << CommandLength::VARIABLE_ID), 0));
}

Genom::Genom(const Genom& rhs)
    : genom_(rhs.genom_),
      minds_(""),
      energy_(rhs.energy_),
      treshhold_energy_(rhs.treshhold_energy_),
      sensor_statuses_(std::move(std::vector(8, touchSensorStatus::Empty))),
      variables_(std::move(std::vector((1 << CommandLength::VARIABLE_ID), 0)))
{
}
Genom& Genom::operator=(const Genom& rhs)
{
    genom_ = rhs.genom_;
    minds_ = "";
    energy_ = rhs.energy_;
    treshhold_energy_ = rhs.treshhold_energy_;
    sensor_statuses_ = std::move(std::vector(8, touchSensorStatus::Empty));
    variables_ = std::move(std::vector((1 << CommandLength::VARIABLE_ID), 0));
    return *this;
}

void Genom::mutation(bool canMutate, double probability)
{
    if (!canMutate) {
        return;
    }
    if (rand_bool(probability)) {
        genom_.Mutate();
    }
}

Operation Genom::nextMove(long long& energy, bool canMutate /* = true */, long long treshhold_energy /* = 0 */,
                          bool rewrite_minds /*= false*/)
{
    treshhold_energy_ = treshhold_energy;
    energy_ = energy;
    // d_energy = 1;
    if (!alive()) {
        minds_.append("I'm already dead! Leave me...");
        return OperationType::DIE;
    }
    cleanMinds(rewrite_minds);
    mutation(canMutate, 0.5);
    return PickNextWorldOperation();
}

Operation Genom::ProcessNextOperation(Operation operation)
{
    Operation result;
    minds_.append(OperationTypeToString.at(operation.type_));

    switch (operation.type_) {
        case OperationType::GOTO:
            parseGoto();
            result = PickNextWorldOperation();
            break;
        case OperationType::PHOTOSYNTESIS:
            result.type_ = OperationType::PHOTOSYNTESIS;
            break;
        case OperationType::TILL:
            result.type_ = OperationType::TILL;
            break;
        case OperationType::CHEMICAL_REACTION:
            result.type_ = OperationType::CHEMICAL_REACTION;
            break;
        case OperationType::WAIT:
            result.type_ = OperationType::WAIT;
            break;
        case OperationType::GO:
            minds_.append(" ");
            result = parseGo();
            break;
        case OperationType::IF:
            minds_.append(" ");
            parseIf();
            result = PickNextWorldOperation();
            break;
        case OperationType::MAKE_CHILD:
            result = parseMakeChild();
            break;
        case OperationType::DIE:
            minds_.append("I wanna die! It's my decision!");
            result.type_ = OperationType::DIE;
            break;
        case OperationType::ASSIGN:
            ParseAssign();
            result = PickNextWorldOperation();
    }
    return result;
}

Operation Genom::PickNextWorldOperation()
{
    if (!alive()) {
        return OperationType::DIE;
    }
    Operation result;
    do {
        OperationType type;
        do {
            type = static_cast<OperationType>(GetNumberFromGenom(CommandLength::OPERATION));
        } while (alive() && OperationTypes.find(type) == OperationTypes.end());

        if (!alive()) {
            return OperationType::DIE;
        }
        result = ProcessNextOperation(type);
    } while (!result.IsWorldOperation() && alive());

    return result;
}

void Genom::parseGoto()
{
    size_t next_position = GetNumberFromGenom(CommandLength::GOTO);
    genom_.SetCurrentBit(next_position);
}

Operation Genom::parseGo()
{
    eDirection direction;
    do {
        direction = static_cast<eDirection>(GetNumberFromGenom(CommandLength::DIRECTION));
    } while (directions.find(direction) == directions.end());

    Operation doNow;
    doNow.type_ = OperationType::GO;
    doNow.direction_ = direction;
    minds_.append(directionsToString.at(direction));
    return doNow;
}

Operation Genom::parseMakeChild()
{
    Operation doNow;
    doNow.type_ = OperationType::MAKE_CHILD;
    return doNow;
}

// IF <bool expr> <goto> <goto>
void Genom::parseIf()
{
    bool condition = parseBoolExpression();
    minds_.append(" THEN ");
    if (condition) {
        parseGoto();
    } else {
        minds_.append("ELSE ");

        // skip goto in case of true
        GetNumberFromGenom(CommandLength::GOTO);
        if (!alive()) {
            return;
        }
        parseGoto();
    }
}

void Genom::ParseAssign()
{
    size_t varId = GetNumberFromGenom(CommandLength::VARIABLE_ID);
    int value = parseExpression();
    minds_.append(" var[").append(std::to_string(varId)).append("]=").append(std::to_string(value));
    variables_[varId] = value;
}
// 0101100101001000010001101001001010101
/*
 * Expression = <exp>
 * <exp> = <MathOperator operation> <exp> <exp>
 * <exp> = <const>
 * <exp> = <sensor status>
 */
int Genom::parseExpression()
{
    if (!alive()) {
        return 0;
    }

    MathOperator nextCommand;
    do {
        nextCommand = static_cast<MathOperator>(GetNumberFromGenom(CommandLength::MATH));
    } while (MathOperators.find(nextCommand) == MathOperators.end() && alive());
    if (!alive()) {
        return 0;
    }

    if (IsReceivingTwoNumerics(nextCommand)) {
        int lhs = parseExpression();
        minds_.append(" ").append(MathOperatorToString.at(nextCommand)).append(" ");
        int rhs = parseExpression();
        return MathOperatorTwoNumerics.at(nextCommand)(lhs, rhs);
    }

    int result = 0;
    size_t touchSensorId, varId;
    switch (nextCommand) {
        case MathOperator::NUMBER_CONST:
            result = GetNumberFromGenom(CommandLength::NUMBER);
            minds_.append(std::to_string(result));
            break;
        case MathOperator::ENERGY:
            result = energy_;
            minds_.append(std::to_string(result));
            break;
        case MathOperator::SENSOR_STATUS:
            touchSensorId = GetNumberFromGenom(CommandLength::SENSOR_STATUS_ID);
            result = static_cast<int>(sensor_statuses_[touchSensorId]);
            minds_.append(touchSensorStatusToString.at(sensor_statuses_[touchSensorId]));
            break;
        case MathOperator::VARIABLE:
            varId = GetNumberFromGenom(CommandLength::VARIABLE_ID);
            result = variables_[varId];
            minds_.append("var[")
                .append(std::to_string(varId))
                .append("](=")
                .append(std::to_string(result))
                .append(")");
            break;
        default:
            std::cout << "ERRREPRPRPERPERPEPREP! " << static_cast<int>(nextCommand) << "\n";
            break;
    }
    return result;
}

/*
 * BoolExpression = <Bexp>
 * <Bexp> = <bool operation> <Bexp> <Bexp>
 * <Bexp> = <bool const>
 */
bool Genom::parseBoolExpression()
{
    int value;

    LogicalOperator nextCommand = static_cast<LogicalOperator>(255);
    do {
        nextCommand = static_cast<LogicalOperator>(GetNumberFromGenom(CommandLength::BOOL));
    } while (alive() && !IsLogicalOperator(nextCommand));

    if (!alive()) {
        return false;
    }
    if (IsReceivingTwoNumerics(nextCommand)) {
        int lhs = parseExpression();
        minds_.append(" ").append(LogicalOperatorToString.at(nextCommand)).append(" ");
        int rhs = parseExpression();
        return LogicalOperatorTwoNumerics.at(nextCommand)(lhs, rhs);
    }

    if (IsReceivingTwoBools(nextCommand)) {
        bool b_lhs = parseBoolExpression();
        minds_.append(" ").append(LogicalOperatorToString.at(nextCommand)).append(" ");
        bool b_rhs = parseBoolExpression();
        return LogicalOperatorTwoBools.at(nextCommand)(b_lhs, b_rhs);
    }

    if (IsReceivingOneBool(nextCommand)) {
        minds_.append(LogicalOperatorToString.at(nextCommand)).append(" ");
        bool lhs = parseBoolExpression();
        return LogicalOperatorOneBool.at(nextCommand)(lhs);
    }

    if (IsReceivingTwoSensors(nextCommand)) {
        touchSensorStatus lhs = static_cast<touchSensorStatus>(parseExpression());
        minds_.append(" ").append(LogicalOperatorToString.at(nextCommand)).append(" ");
        touchSensorStatus rhs = static_cast<touchSensorStatus>(parseExpression());
        return LogicalOperatorTwoSensors.at(nextCommand)(lhs, rhs);
    }

    if (nextCommand == LogicalOperator::BOOL_CONST) {
        bool lhs = GetNumberFromGenom(CommandLength::CONST_BOOL);
        minds_.append(LogicalOperatorToString.at(nextCommand)).append(lhs ? "(TRUE)" : "(FALSE)");
        return lhs;
    }
    return false;
}

std::string Genom::getMinds() const { return minds_; }

void Genom::setTouchSensorsStatuses(const std::vector<touchSensorStatus>& stats) { sensor_statuses_ = stats; }

bool Genom::alive() const { return energy_ > treshhold_energy_; }

void Genom::cleanMinds(bool rewriteMinds)
{
    if (rewriteMinds) {
        minds_.clear();
    } else {
        if (!minds_.empty()) minds_.append("\n");
    }
}

size_t Genom::GetNumberFromGenom(Length length)
{
    energy_--;
    //    d_energy++;
    return genom_.GetNumber(length);
}
