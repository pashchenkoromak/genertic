#ifndef GENOM_HPP
#define GENOM_HPP

#include <bitset>
#include <string>
#include <vector>

#include "genomContainer.hpp"
#include "operation.hpp"

/// @class Genom
/// @brief Genom stores a sequence of 0 and 1 and parses it.
class Genom
{
public:
    /// @brief constructor. It makes new genom, randomly filled commands.
    Genom();

    /// @brief Doesn't copy minds
    /// @param rhs
    Genom(const Genom& rhs);
    Genom& operator=(const Genom& rhs);

    /// @brief default destructor
    virtual ~Genom() = default;

    /// @brief Pick one of possible mutations
    /// @result changes in genom
    virtual void mutation(bool canMutate = true, double probability = 1);

    virtual std::string getMinds() const;
    virtual void cleanMinds(bool rewriteMinds);

    /// @brief makes next move, due to genom.
    /// @param[in] canMutate - on true, with a small probability genom will mutate.
    /// @param[out] energy - count of thinking iterations cannot be more, than energy.
    /// If energy is < treshhold_energy - must return operation 'die'.
    /// @return Operation struct with command to the World.
    virtual Operation nextMove(long long& energy, bool canMutate = true, long long treshhold_energy = 0,
                               bool rewrite_minds = true);

    /// @brief set touch sensors statuses.
    /// @param[in] stats - vector of 8 statuses.
    virtual void setTouchSensorsStatuses(const std::vector<touchSensorStatus>& stats);

    /// @brief Get amount of bits in genom
    size_t size() const { return genom_.size(); }
    /// @brief Get genom as string of 0 and 1. "001100" - like this.
    /// @return May be empty
    std::string GetGenomString() const { return genom_.ToString(); }

protected:
    /// @brief Return the next world operation from genom
    /// @return
    Operation PickNextWorldOperation();
    /// @brief Processes any operation, including IF and GOTO
    /// @param operation
    /// @return
    Operation ProcessNextOperation(Operation operation);

    size_t GetNumberFromGenom(Length length);

    /// @brief set next command starting point to bit number
    /// @param bitNumber
    void SetNextCommandPosition(size_t bitNumber);

    /// @brief parses info after goto command in genom
    /// @note changes m_nextCommand
    void parseGoto();

    /// @brief parses GO command. It needs additional info - direction.
    /// @return Operation GO with direction in params.
    Operation parseGo();

    /// @brief parses MAKE_CHILD command. It for now doesn't need additional info,
    /// but it is possible.
    /// @return Operation MAKE_CHILD.
    Operation parseMakeChild();

    /// @brief Parse IF command. See more info in operation.hpp file.
    /// @note changes m_nextCommand
    void parseIf();

    /// @brief vars[variable_id] = expression
    void ParseAssign();

    /// @brief Parses expression.
    /// @return Result of expression.
    int parseExpression();

    /// @brief Parses bool expression.
    /// @return Result of expression.
    bool parseBoolExpression();

    /// @brief Check if unit is still alive
    /// @return true - if alive
    bool alive() const;

    /// @brief Started size of genom (it can be a little more).
    static constexpr size_t DEFAULT_SIZE = 32;

    GenomContainer genom_;

    /// @brief Description of the way of thinking
    std::string minds_;

    /// @brief energy for changing during one iteration
    long long energy_;
    long long treshhold_energy_;
    long long d_energy;
    /// @brief touch sensor statuses. Set every turn by world.
    std::vector<touchSensorStatus> sensor_statuses_;
    std::vector<int> variables_;
};

#endif // GENOM_HPP
