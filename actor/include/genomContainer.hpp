#pragma once
#include <bitset>
#include <vector>

#include "operation.hpp"

constexpr size_t MaxMutationLength = 7;
enum ChromosomeMutationType
{
    Double,
    Reverse,
    Erase
};

class GenomContainer
{
public:
    GenomContainer() = delete;
    GenomContainer(size_t number, Length length);

    /// @brief Get number beginning from the currentBit position
    /// @param length - amount of bits to proceed
    /// @return
    size_t GetNumber(Length length);

    /// @brief Insert a number into the genom
    /// @param where - bit position to insert (if it is more than th length of genom - take it as where % genom.size() )
    /// @param number - what to insert in binary form
    /// @param length - how many bits have to store it (zeros will be added)
    void Insert(Position where, size_t number, Length length);

    void Append(size_t number, Length length);

    /// @brief Remove a part of genom between where and where + length.
    /// @param where
    /// @param length
    void Remove(Position where, Length length);

    std::vector<bool> ConvertNumToVec(size_t number, Length length);

    void SetCurrentBit(Position where);
    size_t GetCurrentBit() const;

    void Mutate();

    std::string ToString() const;
    size_t size() const;

protected:
    /// @brief change count of genes
    void genomMutation(size_t number, Length length);

    /// @brief small mutation of small count of genes
    void genMutation(Position where, size_t number, Length length);

    /// @brief the biggest mutation
    void chromosomeMutation(ChromosomeMutationType type, Position where, Length length);

    /// @brief doubles some small part of genom
    void doubleGen(Position where, Length length);

    /// @brief erase some small part of genom
    void eraseGen(Position where, Length length);

    /// @brief reverse small part of genom
    void reverseGen(Position where, Length length);

    size_t GetNumberFromPos(Position where, Length length) const;

    static std::string VecToString(const std::vector<bool>& vec);

    std::vector<bool> genom_;
    /// @brief current bit number in genom element (up to BATCH_BIT_SIZE)
    size_t current_bit_;
};
