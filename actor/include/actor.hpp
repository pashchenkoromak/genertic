#ifndef ACTOR_HPP
#define ACTOR_HPP

#include <iostream>
#include <string>
#include <vector>

#include "genom.hpp"
#include "operation.hpp"

/// @class Actor
/// @brief includes genom and some additional info, that could be used by World.
class Actor
{
public:
    /// @brief starting energy for every actor by default
    static const long long START_ENERGY = 400;

    /// @brief construct actor with some energy.
    /// @param[in] energy - start energy.
    /// @note Every actor has unique id_.
    Actor(const long long energy = START_ENERGY);
    /// @brief Copy actor.
    /// @param[in] rhs - base actor.
    /// @note Be care: you'll have 2 actors with equals id_
    Actor(const Actor& rhs) = default;
    /// @brief move constructor
    Actor(Actor&& rhs) = default;
    Actor(const Genom& rhs);
    /// @brief operator =
    /// @param[in] rhs - base actor
    /// @result - the same actor with the same id_.
    Actor& operator=(const Actor& rhs) = default;
    /// @brief move assignment operator=
    /// @param[in] rhs - base actor
    /// @result - the same actor with the same id_.
    Actor& operator=(Actor&& rhs) = default;
    int getId() const { return id_; }
    virtual std::string getMinds() const;

    /// @brief mutation of genom
    /// @result some changes in genom
    virtual void mutation();

    /// @brief get next move of actor
    /// @return Operation to the World
    virtual void prepareNextMove();

    /// @brief Make this actor a corpse
    virtual void die();

    /// @brief changes energy for value
    /// @param[in] value - adding to the energy value
    /// @note value can be <0
    void changeEnergy(const long long value);

    /// @brief set touch sensors statuses.
    /// @param[in] stats - vector of 8 statuses.
    void setTouchSensorsStatuses(const std::vector<touchSensorStatus>& stats);

    /// @brief operators == and !=
    /// @note it checking equality only using id_.
    /// @{
    friend bool operator==(const Actor& lhs, const Actor& rhs);
    friend bool operator!=(const Actor& lhs, const Actor& rhs);
    /// @}

    bool IsCorpse() const;

    /// @brief Return actor with id_ = -1
    static Actor NO_UNIT();
    /// @brief Return actor with id_ = -2
    static Actor CORPSE();

    /// @brief Energy getter
    /// @return current energy of actor
    long long getEnergy() const;

    /// @brief Age getter
    /// @return current actor age
    long long getAge() const;

    size_t getGenomLength() const;

    /// @brief makes new actor. It`s the same as base actor, but with other energy, id_ and 0 age.
    /// @param[in] rhs - base actor
    /// @param[in] - start energy
    static std::shared_ptr<Actor> Child(const Actor& rhs, const long long energy);

    Operation GetNextMove() const;
    void SetNextMove(const Operation& nextMove);

private:
    /// @brief next free id_
    static int nextId;

    /// @brief current actor id_
    int id_;

    /// @brief age of actor
    size_t age_;

    /// @brief energy of actor
    long long energy_;

    /// @brief genom
    Genom genom_;

    Operation next_action_;
};

#endif // UNIT_HPP
