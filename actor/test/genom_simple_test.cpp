#include "common/common.hpp"
#include "genomContainer.hpp"
#include "gtest/gtest.h"

TEST(GenomParse, ParseNumber)
{
    size_t number = 3;
    GenomContainer gen(0, Length(0));
    appender(gen, make_pair(number, CommandLength::GOTO));

    testGenom tgen;

    tgen.setGenom(gen);

    EXPECT_EQ(tgen.GetNumber(CommandLength::GOTO), 3);
    EXPECT_EQ(tgen.GetNextPosition(), 0);
}

TEST(GenomParse, ParseGoto)
{
    long long TEST_START_ENERGY = 20;
    size_t next_pos = 3;
    GenomContainer gen(0, Length(0));
    appender(gen, make_pair(OperationType::GOTO, CommandLength::OPERATION), make_pair(next_pos, CommandLength::GOTO));

    testGenom tgen;

    tgen.setGenom(gen);
    tgen.testGoto(TEST_START_ENERGY, 0);

    EXPECT_EQ(tgen.GetEnergy(), TEST_START_ENERGY - 2);
    EXPECT_EQ(tgen.GetNextPosition(), 3);
}
