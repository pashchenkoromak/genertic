#include "common/common.hpp"
#include "genomContainer.hpp"
#include "gtest/gtest.h"

TEST(GenomVariable, GetDefaultValue)
{
    {
        testGenom tgen;
        GenomContainer gen(0, Length(0));

        appender(gen, make_pair(MathOperator::VARIABLE, CommandLength::MATH), make_pair(0, CommandLength::VARIABLE_ID));
        long long energy = START_ENERGY;
        tgen.setGenom(gen);
        int result = tgen.testParseExpression(energy);
        EXPECT_EQ(result, 0);
        EXPECT_EQ(tgen.GetEnergy(), START_ENERGY - 2);
        EXPECT_EQ(tgen.GetNextPosition(), 0);
        EXPECT_EQ(tgen.getMinds(), "var[0](=0)");
    }
    {
        testGenom tgen;
        GenomContainer gen(0, Length(0));

        appender(gen, make_pair(MathOperator::VARIABLE, CommandLength::MATH), make_pair(7, CommandLength::VARIABLE_ID));
        long long energy = START_ENERGY;
        tgen.setGenom(gen);
        int result = tgen.testParseExpression(energy);
        EXPECT_EQ(result, 0);
        EXPECT_EQ(tgen.GetEnergy(), START_ENERGY - 2);
        EXPECT_EQ(tgen.GetNextPosition(), 0);
        EXPECT_EQ(tgen.getMinds(), "var[7](=0)");
    }
}
TEST(GenomVariable, GetAssignedValue)
{
    {
        testGenom tgen;
        GenomContainer gen(0, Length(0));

        appender(gen, make_pair(OperationType::ASSIGN, CommandLength::OPERATION),
                 make_pair(0, CommandLength::VARIABLE_ID), make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH),
                 make_pair(1, CommandLength::NUMBER), make_pair(OperationType::WAIT, CommandLength::OPERATION),
                 make_pair(MathOperator::VARIABLE, CommandLength::MATH), make_pair(0, CommandLength::VARIABLE_ID));
        size_t finalPos = 0;

        long long energy = START_ENERGY;
        tgen.setGenom(gen);
        EXPECT_EQ(tgen.TestNextMove(energy, 0), Operation(OperationType::WAIT));
        energy = tgen.GetEnergy();
        int result = tgen.testParseExpression(energy);
        EXPECT_EQ(result, 1);
        EXPECT_EQ(tgen.GetEnergy(), START_ENERGY - 7);
        EXPECT_EQ(tgen.GetNextPosition(), 0);
    }
}
