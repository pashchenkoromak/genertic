#include "common/common.hpp"
#include "genomContainer.hpp"
#include "gtest/gtest.h"

class TestContainer : public GenomContainer
{
public:
    TestContainer(size_t number, Length length) : GenomContainer(number, length) {}
    size_t GetNextBitPos() const { return current_bit_; }
    void SetNextBitPos(const size_t newBitPos) { current_bit_ = newBitPos; }
    size_t GetSize() const { return genom_.size(); }
    void CallGenomMutation(size_t number, Length length) { genomMutation(number, length); }
    void CallGenMutation(Position where, size_t number, Length length) { genMutation(where, number, length); }
    void CallChromosomeMutation(ChromosomeMutationType type, Position where, Length length)
    {
        chromosomeMutation(type, where, length);
    }
    void CallDoubleGen(Position where, Length length) { doubleGen(where, length); }
    void CallEraseGen(Position where, Length length) { eraseGen(where, length); }
    void CallReverseGen(Position where, Length length) { reverseGen(where, length); }
};

TEST(GenomContainerTest, contructor)
{
    TestContainer test(7, Length(5));
    EXPECT_EQ(test.GetSize(), 5);
    EXPECT_EQ(test.ToString(), "00111");
}
TEST(GenomContainerTest, GetNumber)
{
    TestContainer test(7, Length(5));
    test.SetNextBitPos(Position(0));
    EXPECT_EQ(test.ToString(), "00111");
    EXPECT_EQ(test.GetNumber(3), 1);
    EXPECT_EQ(test.GetNextBitPos(), 3);
    EXPECT_EQ(test.GetNumber(3), 6);
    EXPECT_EQ(test.GetNextBitPos(), 1);
    test.SetNextBitPos(Position(6));
    EXPECT_EQ(test.GetNumber(3), 3);
    EXPECT_EQ(test.GetNextBitPos(), 4);
}
TEST(GenomContainerTest, Insert)
{
    TestContainer test(7, Length(5));
    EXPECT_EQ(test.ToString(), "00111");
    test.Insert(Position(0), 6, Length(4));
    EXPECT_EQ(test.ToString(), "011000111");
    test.Insert(Position(1), 1, Length(3));
    EXPECT_EQ(test.ToString(), "000111000111");
    test.Insert(Position(12), 0, Length(4));
    EXPECT_EQ(test.ToString(), "0001110001110000");
}
TEST(GenomContainerTest, RemoveNormal)
{
    TestContainer test(7, Length(5));
    EXPECT_EQ(test.ToString(), "00111");
    test.Remove(Position(1), Length(3));
    EXPECT_EQ(test.ToString(), "01");
    test.Remove(Position(1), Length(1));
    EXPECT_EQ(test.ToString(), "0");
    test.Remove(Position(0), Length(1));
    EXPECT_EQ(test.ToString(), "");
}
TEST(GenomContainerTest, RemoveMore)
{
    TestContainer test(7, Length(5));
    EXPECT_EQ(test.ToString(), "00111");
    test.Remove(Position(3), Length(3));
    EXPECT_EQ(test.ToString(), "01");
    test.Remove(Position(3), Length(1));
    EXPECT_EQ(test.ToString(), "0");
    test.Remove(Position(0), Length(1000));
    EXPECT_EQ(test.ToString(), "");
    test.Remove(Position(1), Length(1));
    EXPECT_EQ(test.ToString(), "");
}
TEST(GenomContainerTest, GenomMutation)
{
    TestContainer test(7, Length(5));
    EXPECT_EQ(test.ToString(), "00111");
    test.CallGenomMutation(7, Length(5));
    EXPECT_EQ(test.ToString(), "0011100111");
    test.CallGenomMutation(7, Length(1));
    EXPECT_EQ(test.ToString(), "00111001111");
    test.CallGenomMutation(2, Length(1));
    EXPECT_EQ(test.ToString(), "001110011110");
}
TEST(GenomContainerTest, GenMutation)
{
    TestContainer test(23, Length(5));
    EXPECT_EQ(test.ToString(), "10111");
    test.CallGenMutation(Position(1), 4, Length(3));
    EXPECT_EQ(test.ToString(), "11001");
    test.CallGenMutation(Position(2), 4, Length(3));
    EXPECT_EQ(test.ToString(), "11100");
    test.CallGenMutation(Position(3), 4, Length(3));
    EXPECT_EQ(test.ToString(), "01110");
}

TEST(GenomContainerTest, GenMutationBigLength)
{
    TestContainer test(23, Length(5));
    EXPECT_EQ(test.ToString(), "10111");
    test.CallGenMutation(Position(0), 24, Length(10));
    EXPECT_EQ(test.ToString(), "11000");
    test.CallGenMutation(Position(2), 1, Length(5));
    EXPECT_EQ(test.ToString(), "01000");
    test.CallGenMutation(Position(2), 1, Length(7));
    EXPECT_EQ(test.ToString(), "00010");
}

TEST(GenomContainerTest, DoubleGen)
{
    TestContainer test(23, Length(5));
    EXPECT_EQ(test.ToString(), "10111");
    test.CallDoubleGen(Position(1), Length(1));
    EXPECT_EQ(test.ToString(), "100111");
    test.CallDoubleGen(Position(2), Length(2));
    EXPECT_EQ(test.ToString(), "10010111");
    test.CallDoubleGen(Position(6), Length(3));
    EXPECT_EQ(test.ToString(), "11110010111");
}
TEST(GenomContainerTest, EraseGen)
{
    TestContainer test(21, Length(5));
    EXPECT_EQ(test.ToString(), "10101");
    test.CallEraseGen(Position(1), Length(1));
    EXPECT_EQ(test.ToString(), "1101");
    test.CallEraseGen(Position(3), Length(2));
    EXPECT_EQ(test.ToString(), "10");
    test.CallEraseGen(Position(1), Length(1));
    EXPECT_EQ(test.ToString(), "1");
    test.CallEraseGen(Position(0), Length(1));
    EXPECT_EQ(test.ToString(), "");
}
TEST(GenomContainerTest, ReverseGen)
{
    TestContainer test(23, Length(5));
    EXPECT_EQ(test.ToString(), "10111");
    test.CallReverseGen(Position(1), Length(2));
    EXPECT_EQ(test.ToString(), "11011");
    test.CallReverseGen(Position(0), Length(3));
    EXPECT_EQ(test.ToString(), "01111");
    test.CallReverseGen(Position(3), Length(4));
    EXPECT_EQ(test.ToString(), "11110");
}
TEST(GenomContainerTest, ChromosomeMutationCall)
{
    TestContainer test(21, Length(5));
    EXPECT_EQ(test.ToString(), "10101");
    test.CallChromosomeMutation(ChromosomeMutationType::Double, 2, 3);
    EXPECT_EQ(test.ToString(), "10110101");
    test.CallChromosomeMutation(ChromosomeMutationType::Reverse, 2, 4);
    EXPECT_EQ(test.ToString(), "10101101");
    test.CallChromosomeMutation(ChromosomeMutationType::Erase, 2, 4);
    EXPECT_EQ(test.ToString(), "1001");
}

TEST(GenomContainerTest, GetNumberEmptyContainer)
{
    TestContainer test(0, Length(0));
    ASSERT_NO_THROW(test.GetNumber(0));
    EXPECT_EQ(test.GetNumber(0), 0);
}

TEST(GenomContainerTest, EmptyContainerMutations)
{
    TestContainer test(0, Length(0));

    ASSERT_NO_THROW(test.CallReverseGen(Position(1), Length(2)));
    ASSERT_NO_THROW(test.CallEraseGen(Position(1), Length(2)));
    ASSERT_NO_THROW(test.CallDoubleGen(Position(1), Length(2)));
    ASSERT_NO_THROW(test.CallGenMutation(Position(1), 4, Length(3)));
    ASSERT_NO_THROW(test.Remove(Position(1), Length(1)));
    ASSERT_NO_THROW(test.size());
    ASSERT_NO_THROW(test.CallGenomMutation(7, Length(5)));
    ASSERT_NO_THROW(test.ToString());
}
TEST(GenomContainerTest, MutateCall)
{
    TestContainer test(0, Length(0));
    for (int i = 0; i < 1000; i++) {
        EXPECT_NO_THROW(test.Mutate());
    }
}