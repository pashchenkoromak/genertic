#include "common/common.hpp"
#include "genomContainer.hpp"
#include "gtest/gtest.h"

long long TEST_START_ENERGY = 20;

void testNextTurn(const GenomContainer& gen, const long long finalEnergy, const Operation& correctResult,
                  std::string output)
{
    testGenom tgen;

    long long energy = TEST_START_ENERGY;
    tgen.setGenom(gen);
    Operation result = tgen.nextMove(energy, false, 0, false);
    if (!output.empty()) {
        EXPECT_EQ(output, tgen.getMinds());
    }
    EXPECT_EQ(result, correctResult) << "Expected " << OperationTypeToString.at(correctResult.type_) << " but "
                                     << OperationTypeToString.at(result.type_) << " received.\n"
                                     << (result == correctResult) << std::endl;
    EXPECT_EQ(tgen.GetEnergy(), finalEnergy);
}

/// IF block
// test simple if
TEST(ParseIFExpression, JustBoolConsts)
{
    const size_t GOTO_TRUE = CommandLength::OPERATION + CommandLength::BOOL + CommandLength::MATH * 2 +
                             CommandLength::NUMBER * 2 + CommandLength::GOTO * 2;
    const size_t GOTO_FALSE = GOTO_TRUE + CommandLength::OPERATION;
    {
        GenomContainer gen(0, Length(0));
        appender(gen, make_pair(OperationType::IF, CommandLength::OPERATION), // 4
                 make_pair(LogicalOperator::MORE, CommandLength::BOOL),       // 4
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH),  // 3
                 make_pair(5, CommandLength::NUMBER),                         // 4
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH),  // 3
                 make_pair(3, CommandLength::NUMBER),                         // 4
                 make_pair(GOTO_TRUE, CommandLength::GOTO),                   // 8
                 make_pair(GOTO_FALSE, CommandLength::GOTO),                  // 8
                 make_pair(OperationType::WAIT, CommandLength::OPERATION),    // 4
                 make_pair(OperationType::TILL, CommandLength::OPERATION)     // 4
        );

        testNextTurn(gen, TEST_START_ENERGY - 8, Operation(OperationType::WAIT), "IF 5 > 3 THEN WAIT");
    }
    {
        GenomContainer gen(0, Length(0));
        appender(gen, make_pair(OperationType::IF, CommandLength::OPERATION), // 4
                 make_pair(LogicalOperator::LESS, CommandLength::BOOL),       // 4
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH),  // 3
                 make_pair(5, CommandLength::NUMBER),                         // 4
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH),  // 3
                 make_pair(3, CommandLength::NUMBER),                         // 4
                 make_pair(GOTO_TRUE, CommandLength::GOTO),                   // 8
                 make_pair(GOTO_FALSE, CommandLength::GOTO),                  // 8
                 make_pair(OperationType::WAIT, CommandLength::OPERATION),    // 4
                 make_pair(OperationType::TILL, CommandLength::OPERATION)     // 4
        );

        testNextTurn(gen, TEST_START_ENERGY - 9, Operation(OperationType::TILL), "IF 5 < 3 THEN ELSE TILL");
    }
}

// test if with expression
TEST(ParseEndless, Must_Die)
{
    {
        GenomContainer gen(0, Length(0));
        appender(gen, make_pair(OperationType::GOTO, CommandLength::OPERATION), make_pair(0, CommandLength::NUMBER));
        Operation die(OperationType::DIE);
        std::string expect_output = "";
        int NO_ENERGY = 0;
        testNextTurn(gen, NO_ENERGY, die, expect_output);
    }
    {
        GenomContainer gen(0, Length(1));
        Operation die(OperationType::DIE);
        std::string expect_output = "";
        int NO_ENERGY = 0;
        testNextTurn(gen, NO_ENERGY, die, expect_output);
    }
}
TEST(ParseIFExpression, if_inside_if)
{
    GenomContainer gen(0, Length(0));

    const size_t TRUE_GOTO_POS = CommandLength::OPERATION + CommandLength::BOOL + CommandLength::MATH * 2 +
                                 CommandLength::NUMBER * 2 + CommandLength::GOTO * 2;
    const size_t TRUE_TRUE_GOTO_POS = CommandLength::OPERATION * 2 + CommandLength::BOOL * 2 + CommandLength::MATH * 4 +
                                      CommandLength::NUMBER * 4 + CommandLength::GOTO * 4;
    const size_t TRUE_FALSE_GOTO_POS = TRUE_TRUE_GOTO_POS + CommandLength::OPERATION;
    const size_t FALSE_FALSE_GOTO_POS = TRUE_TRUE_GOTO_POS + CommandLength::OPERATION * 2;
    appender(gen, make_pair(OperationType::IF, CommandLength::OPERATION), // 4
             make_pair(LogicalOperator::MORE, CommandLength::BOOL),       // 4 = 8
             make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH),  // 3 = 11
             make_pair(5, CommandLength::NUMBER),                         // 4 = 15
             make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH),  // 3 = 18
             make_pair(3, CommandLength::NUMBER),                         // 4 = 22
             make_pair(TRUE_GOTO_POS, CommandLength::GOTO),               // 8 = 30
             make_pair(FALSE_FALSE_GOTO_POS, CommandLength::GOTO),        // 8 = 38
             make_pair(OperationType::IF, CommandLength::OPERATION),      // 4 = 42
             make_pair(LogicalOperator::MORE, CommandLength::BOOL),       // 4 = 46
             make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH),  // 3 = 49
             make_pair(5, CommandLength::NUMBER),                         // 4 = 53
             make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH),  // 3 = 56
             make_pair(3, CommandLength::NUMBER),                         // 4 = 60
             make_pair(TRUE_TRUE_GOTO_POS, CommandLength::GOTO),          // 8 = 68
             make_pair(TRUE_FALSE_GOTO_POS, CommandLength::GOTO),         // 8 = 76
             make_pair(OperationType::WAIT, CommandLength::OPERATION),    // 4  TRUE_TRUE_GOTO_POS
             make_pair(OperationType::TILL, CommandLength::OPERATION),    // 4 = 84
             make_pair(OperationType::DIE, CommandLength::OPERATION)      // 4 = 88
    );
    testNextTurn(gen, TEST_START_ENERGY - 15, Operation(OperationType::WAIT), "IF 5 > 3 THEN IF 5 > 3 THEN WAIT");
}

TEST(ParseIFExpression, IfBoolConstant)
{
    const size_t GOTO_TRUE =
        CommandLength::OPERATION + CommandLength::BOOL + CommandLength::CONST_BOOL + CommandLength::GOTO * 2;
    const size_t GOTO_FALSE = GOTO_TRUE + CommandLength::OPERATION;
    {
        GenomContainer gen(0, Length(0));
        appender(gen, make_pair(OperationType::IF, CommandLength::OPERATION), // 4
                 make_pair(LogicalOperator::BOOL_CONST, CommandLength::BOOL), // 4
                 make_pair(1, CommandLength::CONST_BOOL),                     // 1
                 make_pair(GOTO_TRUE, CommandLength::GOTO),                   // 8
                 make_pair(GOTO_FALSE, CommandLength::GOTO),                  // 8
                 make_pair(OperationType::WAIT, CommandLength::OPERATION),    // 4
                 make_pair(OperationType::TILL, CommandLength::OPERATION)     // 4
        );

        testNextTurn(gen, TEST_START_ENERGY - 5, Operation(OperationType::WAIT), "IF GetBoolConst(TRUE) THEN WAIT");
    }
    {
        GenomContainer gen(0, Length(0));
        appender(gen, make_pair(OperationType::IF, CommandLength::OPERATION), // 4
                 make_pair(LogicalOperator::BOOL_CONST, CommandLength::BOOL), // 4
                 make_pair(0, CommandLength::CONST_BOOL),                     // 3
                 make_pair(GOTO_TRUE, CommandLength::GOTO),                   // 8
                 make_pair(GOTO_FALSE, CommandLength::GOTO),                  // 8
                 make_pair(OperationType::WAIT, CommandLength::OPERATION),    // 4
                 make_pair(OperationType::TILL, CommandLength::OPERATION)     // 4
        );

        testNextTurn(gen, TEST_START_ENERGY - 6, Operation(OperationType::TILL),
                     "IF GetBoolConst(FALSE) THEN ELSE TILL");
    }
}

TEST(ParseIFExpression, IfBoolWaste)
{
    const size_t GOTO_TRUE =
        CommandLength::OPERATION + CommandLength::BOOL * 2 + CommandLength::CONST_BOOL + CommandLength::GOTO * 2;
    const size_t GOTO_FALSE = GOTO_TRUE + CommandLength::OPERATION;
    {
        GenomContainer gen(0, Length(0));
        appender(gen, make_pair(OperationType::IF, CommandLength::OPERATION), // 4
                 make_pair(15, CommandLength::BOOL),                          // 4
                 make_pair(LogicalOperator::BOOL_CONST, CommandLength::BOOL), // 4
                 make_pair(1, CommandLength::CONST_BOOL),                     // 3
                 make_pair(GOTO_TRUE, CommandLength::GOTO),                   // 8
                 make_pair(GOTO_FALSE, CommandLength::GOTO),                  // 8
                 make_pair(OperationType::WAIT, CommandLength::OPERATION),    // 4
                 make_pair(OperationType::TILL, CommandLength::OPERATION)     // 4
        );

        testNextTurn(gen, TEST_START_ENERGY - 6, Operation(OperationType::WAIT), "IF GetBoolConst(TRUE) THEN WAIT");
    }
    {
        GenomContainer gen(0, Length(0));
        appender(gen, make_pair(OperationType::IF, CommandLength::OPERATION), // 4
                 make_pair(15, CommandLength::BOOL),                          // 4
                 make_pair(15, CommandLength::BOOL),                          // 4
                 make_pair(15, CommandLength::BOOL),                          // 4
                 make_pair(15, CommandLength::BOOL),                          // 4
                 make_pair(LogicalOperator::BOOL_CONST, CommandLength::BOOL), // 4
                 make_pair(0, CommandLength::CONST_BOOL),
                 make_pair(GOTO_TRUE + CommandLength::BOOL * 3, CommandLength::GOTO),  // 8
                 make_pair(GOTO_FALSE + CommandLength::BOOL * 3, CommandLength::GOTO), // 8
                 make_pair(OperationType::WAIT, CommandLength::OPERATION),             // 4
                 make_pair(OperationType::TILL, CommandLength::OPERATION)              // 4
        );

        testNextTurn(gen, TEST_START_ENERGY - 10, Operation(OperationType::TILL),
                     "IF GetBoolConst(FALSE) THEN ELSE TILL");
    }
}

TEST(ParseIFExpression, IfBoolAnd)
{
    const size_t GOTO_TRUE =
        CommandLength::OPERATION + CommandLength::BOOL * 3 + CommandLength::CONST_BOOL * 2 + CommandLength::GOTO * 2;
    const size_t GOTO_FALSE = GOTO_TRUE + CommandLength::OPERATION;
    {
        GenomContainer gen(0, Length(0));
        appender(gen, make_pair(OperationType::IF, CommandLength::OPERATION),
                 make_pair(LogicalOperator::AND, CommandLength::BOOL),
                 make_pair(LogicalOperator::BOOL_CONST, CommandLength::BOOL), make_pair(1, CommandLength::CONST_BOOL),
                 make_pair(LogicalOperator::BOOL_CONST, CommandLength::BOOL), make_pair(1, CommandLength::CONST_BOOL),
                 make_pair(GOTO_TRUE, CommandLength::GOTO),                // 8
                 make_pair(GOTO_FALSE, CommandLength::GOTO),               // 8
                 make_pair(OperationType::WAIT, CommandLength::OPERATION), // 4
                 make_pair(OperationType::TILL, CommandLength::OPERATION)  // 4
        );

        testNextTurn(gen, TEST_START_ENERGY - 8, Operation(OperationType::WAIT),
                     "IF GetBoolConst(TRUE) AND GetBoolConst(TRUE) THEN WAIT");
    }
    {
        GenomContainer gen(0, Length(0));
        appender(gen, make_pair(OperationType::IF, CommandLength::OPERATION),
                 make_pair(LogicalOperator::AND, CommandLength::BOOL),
                 make_pair(LogicalOperator::BOOL_CONST, CommandLength::BOOL), make_pair(1, CommandLength::CONST_BOOL),
                 make_pair(LogicalOperator::BOOL_CONST, CommandLength::BOOL), make_pair(0, CommandLength::CONST_BOOL),
                 make_pair(GOTO_TRUE, CommandLength::GOTO),                // 8
                 make_pair(GOTO_FALSE, CommandLength::GOTO),               // 8
                 make_pair(OperationType::WAIT, CommandLength::OPERATION), // 4
                 make_pair(OperationType::TILL, CommandLength::OPERATION)  // 4
        );

        testNextTurn(gen, TEST_START_ENERGY - 9, Operation(OperationType::TILL),
                     "IF GetBoolConst(TRUE) AND GetBoolConst(FALSE) THEN ELSE TILL");
    }
}

TEST(ParseIFExpression, IfEndless)
{
    const size_t GOTO_TRUE = 0;
    const size_t GOTO_FALSE = 0;
    {
        GenomContainer gen(0, Length(0));
        appender(gen, make_pair(OperationType::IF, CommandLength::OPERATION),
                 make_pair(LogicalOperator::BOOL_CONST, CommandLength::BOOL), make_pair(1, CommandLength::CONST_BOOL),
                 make_pair(GOTO_TRUE, CommandLength::GOTO));

        testNextTurn(gen, 0, Operation(OperationType::DIE), "");
    }
    {
        GenomContainer gen(0, Length(0));
        appender(gen, make_pair(OperationType::IF, CommandLength::OPERATION),
                 make_pair(LogicalOperator::BOOL_CONST, CommandLength::BOOL), make_pair(0, CommandLength::CONST_BOOL),
                 make_pair(GOTO_TRUE, CommandLength::GOTO), make_pair(GOTO_FALSE, CommandLength::GOTO));

        testNextTurn(gen, 0, Operation(OperationType::DIE), "");
    }
}
TEST(ParseIFExpression, NoExcepts)
{
    testGenom tgen;

    for (size_t len = 0; len < 10; len++) {
        for (int i = 0; i < (1 << len); i++) {
            GenomContainer gen(i, Length(len));
            tgen.setGenom(gen);
            tgen.SetNextPosition(0);
            long long energy = 100;
            EXPECT_NO_THROW(tgen.nextMove(energy, false, 0, true));
        }
    }
}
