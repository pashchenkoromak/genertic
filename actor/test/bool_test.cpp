#include "common/common.hpp"
#include "gtest/gtest.h"

/// Bool expressions test block

void testBool(const GenomContainer& gen, const size_t resultPos, const long long finalEnergy, const bool correctResult,
              const std::string& output)
{
    testGenom tgen;
    long long energy = START_ENERGY;
    tgen.setGenom(gen);
    bool result = tgen.testParseBoolExpression(energy);
    EXPECT_EQ(tgen.GetNextPosition(), resultPos);
    EXPECT_EQ(result, correctResult);
    EXPECT_EQ(tgen.GetEnergy(), finalEnergy);
    EXPECT_EQ(output, tgen.getMinds());
}

// test const bool
TEST(ParseBoolExpression, Const)
{
    {
        GenomContainer gen(0, Length(0));
        appender(gen, make_pair(LogicalOperator::BOOL_CONST, CommandLength::BOOL),
                 make_pair(false, CommandLength::CONST_BOOL));
        size_t finalPos = 0;
        testBool(gen, finalPos, START_ENERGY - 2, false, "GetBoolConst(FALSE)");
    }
    {
        GenomContainer gen(0, Length(0));
        appender(gen, make_pair(LogicalOperator::BOOL_CONST, CommandLength::BOOL),
                 make_pair(true, CommandLength::CONST_BOOL));
        size_t finalPos = 0;
        testBool(gen, finalPos, START_ENERGY - 2, true, "GetBoolConst(TRUE)");
    }
}

// test and operator bool
TEST(ParseBoolExpression, And)
{
    {
        GenomContainer gen(0, Length(0));
        appender(
            gen, make_pair(LogicalOperator::AND, CommandLength::BOOL),
            make_pair(LogicalOperator::BOOL_CONST, CommandLength::BOOL), make_pair(false, CommandLength::CONST_BOOL),
            make_pair(LogicalOperator::BOOL_CONST, CommandLength::BOOL), make_pair(true, CommandLength::CONST_BOOL));
        size_t finalPos = 0;
        testBool(gen, finalPos, START_ENERGY - 5, false, "GetBoolConst(FALSE) AND GetBoolConst(TRUE)");
    }
    {
        GenomContainer gen(0, Length(0));
        appender(
            gen, make_pair(LogicalOperator::AND, CommandLength::BOOL),
            make_pair(LogicalOperator::BOOL_CONST, CommandLength::BOOL), make_pair(true, CommandLength::CONST_BOOL),
            make_pair(LogicalOperator::BOOL_CONST, CommandLength::BOOL), make_pair(true, CommandLength::CONST_BOOL));
        size_t finalPos = 0;
        testBool(gen, finalPos, START_ENERGY - 5, true, "GetBoolConst(TRUE) AND GetBoolConst(TRUE)");
    }
}

// test or operator bool
TEST(ParseBoolExpression, Or)
{
    {
        GenomContainer gen(0, Length(0));
        appender(
            gen, make_pair(LogicalOperator::OR, CommandLength::BOOL),
            make_pair(LogicalOperator::BOOL_CONST, CommandLength::BOOL), make_pair(false, CommandLength::CONST_BOOL),
            make_pair(LogicalOperator::BOOL_CONST, CommandLength::BOOL), make_pair(true, CommandLength::CONST_BOOL));
        size_t finalPos = 0;
        testBool(gen, finalPos, START_ENERGY - 5, true, "GetBoolConst(FALSE) OR GetBoolConst(TRUE)");
    }
    {
        GenomContainer gen(0, Length(0));
        appender(
            gen, make_pair(LogicalOperator::OR, CommandLength::BOOL),
            make_pair(LogicalOperator::BOOL_CONST, CommandLength::BOOL), make_pair(false, CommandLength::CONST_BOOL),
            make_pair(LogicalOperator::BOOL_CONST, CommandLength::BOOL), make_pair(false, CommandLength::CONST_BOOL));
        size_t finalPos = 0;
        testBool(gen, finalPos, START_ENERGY - 5, false, "GetBoolConst(FALSE) OR GetBoolConst(FALSE)");
    }
}

// test More operator bool
TEST(ParseBoolExpression, More)
{
    {
        GenomContainer gen(0, Length(0));
        appender(gen, make_pair(LogicalOperator::MORE, CommandLength::BOOL),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(2, CommandLength::NUMBER),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(1, CommandLength::NUMBER));
        size_t finalPos = 0;
        testBool(gen, finalPos, START_ENERGY - 5, true, "2 > 1");
    }
    {
        GenomContainer gen(0, Length(0));
        appender(gen, make_pair(LogicalOperator::MORE, CommandLength::BOOL),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(1, CommandLength::NUMBER),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(2, CommandLength::NUMBER));
        size_t finalPos = 0;
        testBool(gen, finalPos, START_ENERGY - 5, false, "1 > 2");
    }
    {
        GenomContainer gen(0, Length(0));
        appender(gen, make_pair(LogicalOperator::MORE, CommandLength::BOOL),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(1, CommandLength::NUMBER),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(1, CommandLength::NUMBER));
        size_t finalPos = 0;
        testBool(gen, finalPos, START_ENERGY - 5, false, "1 > 1");
    }
}

// test Less operator bool
TEST(ParseBoolExpression, Less)
{
    {
        GenomContainer gen(0, Length(0));
        appender(gen, make_pair(LogicalOperator::LESS, CommandLength::BOOL),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(2, CommandLength::NUMBER),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(1, CommandLength::NUMBER));
        size_t finalPos = 0;
        testBool(gen, finalPos, START_ENERGY - 5, false, "2 < 1");
    }
    {
        GenomContainer gen(0, Length(0));
        appender(gen, make_pair(LogicalOperator::LESS, CommandLength::BOOL),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(1, CommandLength::NUMBER),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(2, CommandLength::NUMBER));
        size_t finalPos = 0;
        testBool(gen, finalPos, START_ENERGY - 5, true, "1 < 2");
    }
    {
        GenomContainer gen(0, Length(0));
        appender(gen, make_pair(LogicalOperator::LESS, CommandLength::BOOL),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(1, CommandLength::NUMBER),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(1, CommandLength::NUMBER));
        size_t finalPos = 0;
        testBool(gen, finalPos, START_ENERGY - 5, false, "1 < 1");
    }
}

// test More_Equal operator bool
TEST(ParseBoolExpression, More_Equal)
{
    {
        GenomContainer gen(0, Length(0));
        appender(gen, make_pair(LogicalOperator::MORE_EQUAL, CommandLength::BOOL),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(2, CommandLength::NUMBER),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(1, CommandLength::NUMBER));
        size_t finalPos = 0;
        testBool(gen, finalPos, START_ENERGY - 5, true, "2 >= 1");
    }
    {
        GenomContainer gen(0, Length(0));
        appender(gen, make_pair(LogicalOperator::MORE_EQUAL, CommandLength::BOOL),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(2, CommandLength::NUMBER),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(2, CommandLength::NUMBER));
        size_t finalPos = 0;
        testBool(gen, finalPos, START_ENERGY - 5, true, "2 >= 2");
    }
    {
        GenomContainer gen(0, Length(0));
        appender(gen, make_pair(LogicalOperator::MORE_EQUAL, CommandLength::BOOL),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(2, CommandLength::NUMBER),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(3, CommandLength::NUMBER));
        size_t finalPos = 0;
        testBool(gen, finalPos, START_ENERGY - 5, false, "2 >= 3");
    }
}

// test Less_Equal operator bool
TEST(ParseBoolExpression, Less_Equal)
{
    {
        GenomContainer gen(0, Length(0));
        appender(gen, make_pair(LogicalOperator::LESS_EQUAL, CommandLength::BOOL),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(2, CommandLength::NUMBER),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(1, CommandLength::NUMBER));
        size_t finalPos = 0;
        testBool(gen, finalPos, START_ENERGY - 5, false, "2 <= 1");
    }
    {
        GenomContainer gen(0, Length(0));
        appender(gen, make_pair(LogicalOperator::LESS_EQUAL, CommandLength::BOOL),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(2, CommandLength::NUMBER),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(2, CommandLength::NUMBER));
        size_t finalPos = 0;
        testBool(gen, finalPos, START_ENERGY - 5, true, "2 <= 2");
    }
    {
        GenomContainer gen(0, Length(0));
        appender(gen, make_pair(LogicalOperator::LESS_EQUAL, CommandLength::BOOL),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(2, CommandLength::NUMBER),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(3, CommandLength::NUMBER));
        size_t finalPos = 0;
        testBool(gen, finalPos, START_ENERGY - 5, true, "2 <= 3");
    }
}

// test No_Equal operator bool
TEST(ParseBoolExpression, No_Equal_NUMERICS)
{
    {
        GenomContainer gen(0, Length(0));
        appender(gen, make_pair(LogicalOperator::NO_EQUAL_NUMERIC, CommandLength::BOOL),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(2, CommandLength::NUMBER),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(1, CommandLength::NUMBER));
        size_t finalPos = 0;
        testBool(gen, finalPos, START_ENERGY - 5, true, "2 != 1");
    }
    {
        GenomContainer gen(0, Length(0));
        appender(gen, make_pair(LogicalOperator::NO_EQUAL_NUMERIC, CommandLength::BOOL),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(2, CommandLength::NUMBER),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(2, CommandLength::NUMBER));
        size_t finalPos = 0;
        testBool(gen, finalPos, START_ENERGY - 5, false, "2 != 2");
    }
    {
        GenomContainer gen(0, Length(0));
        appender(gen, make_pair(LogicalOperator::NO_EQUAL_NUMERIC, CommandLength::BOOL),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(2, CommandLength::NUMBER),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(3, CommandLength::NUMBER));
        size_t finalPos = 0;
        testBool(gen, finalPos, START_ENERGY - 5, true, "2 != 3");
    }
}

TEST(ParseBoolExpression, EQUAL_BOOLS)
{
    {
        GenomContainer gen(0, Length(0));
        appender(gen, make_pair(LogicalOperator::EQUAL_BOOLS, CommandLength::BOOL),
                 make_pair(LogicalOperator::BOOL_CONST, CommandLength::BOOL), make_pair(1, CommandLength::CONST_BOOL),
                 make_pair(LogicalOperator::BOOL_CONST, CommandLength::BOOL), make_pair(0, CommandLength::CONST_BOOL));
        size_t finalPos = 0;
        testBool(gen, finalPos, START_ENERGY - 5, false, "GetBoolConst(TRUE) == GetBoolConst(FALSE)");
    }
    {
        GenomContainer gen(0, Length(0));
        appender(gen, make_pair(LogicalOperator::EQUAL_BOOLS, CommandLength::BOOL),
                 make_pair(LogicalOperator::BOOL_CONST, CommandLength::BOOL), make_pair(1, CommandLength::CONST_BOOL),
                 make_pair(LogicalOperator::BOOL_CONST, CommandLength::BOOL), make_pair(1, CommandLength::CONST_BOOL));
        size_t finalPos = 0;
        testBool(gen, finalPos, START_ENERGY - 5, true, "GetBoolConst(TRUE) == GetBoolConst(TRUE)");
    }
    {
        GenomContainer gen(0, Length(0));
        appender(gen, make_pair(LogicalOperator::EQUAL_BOOLS, CommandLength::BOOL),
                 make_pair(LogicalOperator::BOOL_CONST, CommandLength::BOOL), make_pair(0, CommandLength::CONST_BOOL),
                 make_pair(LogicalOperator::BOOL_CONST, CommandLength::BOOL), make_pair(0, CommandLength::CONST_BOOL));
        size_t finalPos = 0;
        testBool(gen, finalPos, START_ENERGY - 5, true, "GetBoolConst(FALSE) == GetBoolConst(FALSE)");
    }
}

// test No_Equal operator bool
TEST(ParseBoolExpression, Equal_NUMERICS)
{
    {
        GenomContainer gen(0, Length(0));
        appender(gen, make_pair(LogicalOperator::EQUAL_NUMERIC, CommandLength::BOOL),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(2, CommandLength::NUMBER),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(1, CommandLength::NUMBER));
        size_t finalPos = 0;
        testBool(gen, finalPos, START_ENERGY - 5, false, "2 == 1");
    }
    {
        GenomContainer gen(0, Length(0));
        appender(gen, make_pair(LogicalOperator::EQUAL_NUMERIC, CommandLength::BOOL),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(2, CommandLength::NUMBER),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(2, CommandLength::NUMBER));
        size_t finalPos = 0;
        testBool(gen, finalPos, START_ENERGY - 5, true, "2 == 2");
    }
    {
        GenomContainer gen(0, Length(0));
        appender(gen, make_pair(LogicalOperator::EQUAL_NUMERIC, CommandLength::BOOL),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(2, CommandLength::NUMBER),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(3, CommandLength::NUMBER));
        size_t finalPos = 0;
        testBool(gen, finalPos, START_ENERGY - 5, false, "2 == 3");
    }
}

TEST(ParseBoolExpression, NO_EQUAL_BOOLS)
{
    {
        GenomContainer gen(0, Length(0));
        appender(gen, make_pair(LogicalOperator::NO_EQUAL_BOOLS, CommandLength::BOOL),
                 make_pair(LogicalOperator::BOOL_CONST, CommandLength::BOOL), make_pair(1, CommandLength::CONST_BOOL),
                 make_pair(LogicalOperator::BOOL_CONST, CommandLength::BOOL), make_pair(0, CommandLength::CONST_BOOL));
        size_t finalPos = 0;
        testBool(gen, finalPos, START_ENERGY - 5, true, "GetBoolConst(TRUE) != GetBoolConst(FALSE)");
    }
    {
        GenomContainer gen(0, Length(0));
        appender(gen, make_pair(LogicalOperator::NO_EQUAL_BOOLS, CommandLength::BOOL),
                 make_pair(LogicalOperator::BOOL_CONST, CommandLength::BOOL), make_pair(1, CommandLength::CONST_BOOL),
                 make_pair(LogicalOperator::BOOL_CONST, CommandLength::BOOL), make_pair(1, CommandLength::CONST_BOOL));
        size_t finalPos = 0;
        testBool(gen, finalPos, START_ENERGY - 5, false, "GetBoolConst(TRUE) != GetBoolConst(TRUE)");
    }
    {
        GenomContainer gen(0, Length(0));
        appender(gen, make_pair(LogicalOperator::NO_EQUAL_BOOLS, CommandLength::BOOL),
                 make_pair(LogicalOperator::BOOL_CONST, CommandLength::BOOL), make_pair(0, CommandLength::CONST_BOOL),
                 make_pair(LogicalOperator::BOOL_CONST, CommandLength::BOOL), make_pair(0, CommandLength::CONST_BOOL));
        size_t finalPos = 0;
        testBool(gen, finalPos, START_ENERGY - 5, false, "GetBoolConst(FALSE) != GetBoolConst(FALSE)");
    }
}

// test No operator bool
TEST(ParseBoolExpression, No)
{
    {
        GenomContainer gen(0, Length(0));
        appender(gen, make_pair(LogicalOperator::NO, CommandLength::BOOL),
                 make_pair(LogicalOperator::BOOL_CONST, CommandLength::BOOL),
                 make_pair(true, CommandLength::CONST_BOOL));
        size_t finalPos = 0;
        testBool(gen, finalPos, START_ENERGY - 3, false, "NO GetBoolConst(TRUE)");
    }
    {
        GenomContainer gen(0, Length(0));
        appender(gen, make_pair(LogicalOperator::NO, CommandLength::BOOL),
                 make_pair(LogicalOperator::BOOL_CONST, CommandLength::BOOL),
                 make_pair(false, CommandLength::CONST_BOOL));
        size_t finalPos = 0;
        testBool(gen, finalPos, START_ENERGY - 3, true, "NO GetBoolConst(FALSE)");
    }
}
