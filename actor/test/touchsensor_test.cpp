#include "common/common.hpp"
#include "genomContainer.hpp"
#include "gtest/gtest.h"

long long STARTING_ENERGY = 100;

struct InputOutput
{
    InputOutput(){};
    size_t resultPos;
    long long startingEnergy;
    long long remainingEnergy;
    Operation result;
    std::string resultOutput;
    std::vector<touchSensorStatus> inputStats;
};

void testExpression(const GenomContainer& gen, InputOutput expected)
{
    testGenom tgen;

    long long energy = START_ENERGY;
    tgen.setGenom(gen);
    ASSERT_EQ(tgen.GetNextPosition(), 0);
    tgen.setTouchSensorsStatuses(expected.inputStats);
    Operation result = tgen.nextMove(energy, false, 0, false);
    size_t next = tgen.GetNextPosition();
    EXPECT_EQ(next, expected.resultPos) << std::endl;
    EXPECT_EQ(result, expected.result) << "Resulted " << result.ToString() << " is not equal to expected "
                                       << expected.result.ToString() << std::endl;
    EXPECT_EQ(tgen.GetEnergy(), expected.remainingEnergy) << std::endl;
    EXPECT_EQ(tgen.getMinds(), expected.resultOutput) << std::endl;
}

TEST(ParseTouchSensor, upleft_upleft)
{
    /*
        LEFT,
        RIGHT,
        UP,
        DOWN,
        UP_LEFT,
        DOWN_LEFT,
        UP_RIGHT,
        DOWN_RIGHT
    */

    std::vector<touchSensorStatus> stats = {
        touchSensorStatus::Empty // left
        ,
        touchSensorStatus::Empty // right
        ,
        touchSensorStatus::Empty // up
        ,
        touchSensorStatus::Corpse // down
        ,
        touchSensorStatus::Corpse,
        touchSensorStatus::Actor,
        touchSensorStatus::Actor,
        touchSensorStatus::Actor // down_right
    };

    const size_t GOTO_TRUE = CommandLength::OPERATION + CommandLength::BOOL + CommandLength::MATH * 2 +
                             CommandLength::DIRECTION * 2 + CommandLength::GOTO * 2;

    const size_t GOTO_FALSE = GOTO_TRUE + CommandLength::OPERATION;

    {
        GenomContainer gen(0, Length(0));
        appender(gen, make_pair(OperationType::IF, CommandLength::OPERATION),
                 make_pair(LogicalOperator::EQUAL_SENSORS, CommandLength::BOOL),
                 make_pair(MathOperator::SENSOR_STATUS, CommandLength::MATH),
                 make_pair(eDirection::LEFT, CommandLength::DIRECTION),
                 make_pair(MathOperator::SENSOR_STATUS, CommandLength::MATH),
                 make_pair(eDirection::RIGHT, CommandLength::DIRECTION), make_pair(GOTO_TRUE, CommandLength::GOTO),
                 make_pair(GOTO_FALSE, CommandLength::GOTO), make_pair(OperationType::WAIT, CommandLength::OPERATION),
                 make_pair(OperationType::TILL, CommandLength::OPERATION),
                 make_pair(OperationType::DIE, CommandLength::OPERATION));
        size_t finalPos = CommandLength::MATH + CommandLength::DIRECTION;
        InputOutput result;
        result.resultPos = GOTO_TRUE + CommandLength::OPERATION;
        result.remainingEnergy = STARTING_ENERGY - 8;
        result.startingEnergy = STARTING_ENERGY;
        result.result = Operation(OperationType::WAIT);
        result.resultOutput = "IF Empty == Empty THEN WAIT";
        result.inputStats = stats;
        testExpression(gen, result);
    }
    {
        GenomContainer gen(0, Length(0));
        appender(gen, make_pair(OperationType::IF, CommandLength::OPERATION),
                 make_pair(LogicalOperator::EQUAL_SENSORS, CommandLength::BOOL),
                 make_pair(MathOperator::SENSOR_STATUS, CommandLength::MATH),
                 make_pair(eDirection::LEFT, CommandLength::DIRECTION),
                 make_pair(MathOperator::SENSOR_STATUS, CommandLength::MATH),
                 make_pair(eDirection::DOWN, CommandLength::DIRECTION), make_pair(GOTO_TRUE, CommandLength::GOTO),
                 make_pair(GOTO_FALSE, CommandLength::GOTO), make_pair(OperationType::WAIT, CommandLength::OPERATION),
                 make_pair(OperationType::TILL, CommandLength::OPERATION),
                 make_pair(OperationType::DIE, CommandLength::OPERATION));
        size_t finalPos = CommandLength::MATH + CommandLength::DIRECTION;
        InputOutput result;
        result.resultPos = GOTO_TRUE + CommandLength::OPERATION * 2;
        result.remainingEnergy = STARTING_ENERGY - 9;
        result.startingEnergy = STARTING_ENERGY;
        result.result = Operation(OperationType::TILL);
        result.resultOutput = "IF Empty == Corpse THEN ELSE TILL";
        result.inputStats = stats;
        testExpression(gen, result);
    }

    {
        GenomContainer gen(0, Length(0));
        appender(gen, make_pair(OperationType::IF, CommandLength::OPERATION),
                 make_pair(LogicalOperator::EQUAL_SENSORS, CommandLength::BOOL),
                 make_pair(MathOperator::SENSOR_STATUS, CommandLength::MATH),
                 make_pair(eDirection::DOWN_RIGHT, CommandLength::DIRECTION),
                 make_pair(MathOperator::SENSOR_STATUS, CommandLength::MATH),
                 make_pair(eDirection::DOWN, CommandLength::DIRECTION), make_pair(GOTO_TRUE, CommandLength::GOTO),
                 make_pair(GOTO_FALSE, CommandLength::GOTO), make_pair(OperationType::WAIT, CommandLength::OPERATION),
                 make_pair(OperationType::TILL, CommandLength::OPERATION),
                 make_pair(OperationType::DIE, CommandLength::OPERATION));
        size_t finalPos = CommandLength::MATH + CommandLength::DIRECTION;
        InputOutput result;
        result.resultPos = GOTO_TRUE + CommandLength::OPERATION * 2;
        result.remainingEnergy = STARTING_ENERGY - 9;
        result.startingEnergy = STARTING_ENERGY;
        result.result = Operation(OperationType::TILL);
        result.resultOutput = "IF Actor == Corpse THEN ELSE TILL";
        result.inputStats = stats;
        testExpression(gen, result);
    }
}
