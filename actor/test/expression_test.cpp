#include "common/common.hpp"
#include "gtest/gtest.h"

void testExpression(const GenomContainer& gen, const size_t resultPos, const long long finalEnergy,
                    const long long correctResult, std::string output)
{
    testGenom tgen;

    long long energy = START_ENERGY;
    tgen.setGenom(gen);
    int result = tgen.testParseExpression(energy);
    EXPECT_EQ(tgen.GetNextPosition(), resultPos);
    EXPECT_EQ(result, correctResult);
    EXPECT_EQ(tgen.GetEnergy(), finalEnergy);
    EXPECT_EQ(output, tgen.getMinds());
}

// test plus parser
TEST(ParseExpression, Const)
{
    {
        GenomContainer gen(0, Length(0));
        appender(gen, make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(1, CommandLength::NUMBER));
        testExpression(gen, 0, START_ENERGY - 2, 1, "1");
    }
    {
        GenomContainer gen(0, Length(0));
        appender(gen, make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(7, CommandLength::NUMBER));
        testExpression(gen, 0, START_ENERGY - 2, 7, "7");
    }
}

// test plus parser
TEST(ParseExpression, Plus)
{
    {
        GenomContainer gen(0, Length(0));
        appender(gen, make_pair(MathOperator::PLUS, CommandLength::MATH),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(1, CommandLength::NUMBER),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(1, CommandLength::NUMBER));
        size_t finalPos = 0;
        testExpression(gen, finalPos, START_ENERGY - 5, 2, "1 + 1");
    }
    {
        GenomContainer gen(0, Length(0));
        appender(gen, make_pair(MathOperator::PLUS, CommandLength::MATH),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(13, CommandLength::NUMBER),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(1, CommandLength::NUMBER));
        size_t finalPos = 0;
        testExpression(gen, finalPos, START_ENERGY - 5, 14, "13 + 1");
    }
}

// test plus parser
TEST(ParseExpression, Minus)
{
    {
        GenomContainer gen(0, Length(0));
        appender(gen, make_pair(MathOperator::MINUS, CommandLength::MATH),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(1, CommandLength::NUMBER),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(1, CommandLength::NUMBER));
        size_t finalPos = 0;
        testExpression(gen, finalPos, START_ENERGY - 5, 0, "1 - 1");
    }
    {
        GenomContainer gen(0, Length(0));
        appender(gen, make_pair(MathOperator::MINUS, CommandLength::MATH),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(6, CommandLength::NUMBER),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(1, CommandLength::NUMBER));
        size_t finalPos = 0;
        testExpression(gen, finalPos, START_ENERGY - 5, 5, "6 - 1");
    }
    {
        GenomContainer gen(0, Length(0));
        appender(gen, make_pair(MathOperator::MINUS, CommandLength::MATH),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(0, CommandLength::NUMBER),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(3, CommandLength::NUMBER));
        size_t finalPos = 0;
        testExpression(gen, finalPos, START_ENERGY - 5, -3, "0 - 3");
    }
}

// test multiple parser
TEST(ParseExpression, Multiple)
{
    {
        GenomContainer gen(0, Length(0));

        appender(gen, make_pair(MathOperator::MULTIPLE, CommandLength::MATH),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(3, CommandLength::NUMBER),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(4, CommandLength::NUMBER));
        size_t finalPos = 0;
        testExpression(gen, finalPos, START_ENERGY - 5, 12, "3 * 4");
    }
    {
        GenomContainer gen(0, Length(0));

        appender(gen, make_pair(MathOperator::MULTIPLE, CommandLength::MATH),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(0, CommandLength::NUMBER),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(4, CommandLength::NUMBER));
        size_t finalPos = 0;
        testExpression(gen, finalPos, START_ENERGY - 5, 0, "0 * 4");
    }
}

// test divide parser
TEST(ParseExpression, Divide)
{
    {
        GenomContainer gen(0, Length(0));

        appender(gen, make_pair(MathOperator::DIVIDE, CommandLength::MATH),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(10, CommandLength::NUMBER),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(2, CommandLength::NUMBER));
        size_t finalPos = 0;
        testExpression(gen, finalPos, START_ENERGY - 5, 5, "10 / 2");
    }
    {
        GenomContainer gen(0, Length(0));

        appender(gen, make_pair(MathOperator::DIVIDE, CommandLength::MATH),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(3, CommandLength::NUMBER),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(2, CommandLength::NUMBER));
        size_t finalPos = 0;
        testExpression(gen, finalPos, START_ENERGY - 5, 1, "3 / 2");
    }
    {
        GenomContainer gen(0, Length(0));

        appender(gen, make_pair(MathOperator::DIVIDE, CommandLength::MATH),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(2, CommandLength::NUMBER),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(2, CommandLength::NUMBER));
        size_t finalPos = 0;
        testExpression(gen, finalPos, START_ENERGY - 5, 1, "2 / 2");
    }
    {
        GenomContainer gen(0, Length(0));

        appender(gen, make_pair(MathOperator::DIVIDE, CommandLength::MATH),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(10, CommandLength::NUMBER),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(0, CommandLength::NUMBER));
        size_t finalPos = 0;
        testExpression(gen, finalPos, START_ENERGY - 5, 0, "10 / 0");
    }
}

// test rest-divide parser
TEST(ParseExpression, Rest_divide)
{
    {
        GenomContainer gen(0, Length(0));

        appender(gen, make_pair(MathOperator::REST_DIVIDE, CommandLength::MATH),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(12, CommandLength::NUMBER),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(3, CommandLength::NUMBER));
        size_t finalPos = 0;
        testExpression(gen, finalPos, START_ENERGY - 5, 0, "12 % 3");
    }
    {
        GenomContainer gen(0, Length(0));

        appender(gen, make_pair(MathOperator::REST_DIVIDE, CommandLength::MATH),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(14, CommandLength::NUMBER),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(3, CommandLength::NUMBER));
        size_t finalPos = 0;
        testExpression(gen, finalPos, START_ENERGY - 5, 2, "14 % 3");
    }
    {
        GenomContainer gen(0, Length(0));

        appender(gen, make_pair(MathOperator::REST_DIVIDE, CommandLength::MATH),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(1, CommandLength::NUMBER),
                 make_pair(MathOperator::NUMBER_CONST, CommandLength::MATH), make_pair(0, CommandLength::NUMBER));
        size_t finalPos = 0;
        testExpression(gen, finalPos, START_ENERGY - 5, 0, "1 % 0");
    }
}

// test getting energy
TEST(ParseExpression, Energy)
{
    GenomContainer gen(0, Length(0));

    appender(gen, make_pair(MathOperator::ENERGY, CommandLength::MATH));
    size_t finalPos = 0;
    testExpression(gen, finalPos, START_ENERGY - 1, 99, "99");
}
