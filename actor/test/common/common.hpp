#include "actor.hpp"

#ifndef COMMON_HPP
#define COMMON_HPP

#include <bitset>

#include "genomContainer.hpp"
#include "worldConfiguration.hpp"

using std::make_pair;

class testGenom : public Genom
{
public:
    testGenom() = default;
    virtual ~testGenom() = default;

    void setGenom(const GenomContainer& rhs) { genom_ = rhs; }

    size_t GetNextPosition() const { return genom_.GetCurrentBit(); }
    void SetNextPosition(Position pos) { genom_.SetCurrentBit(pos); }
    size_t GetNumber(Length length) { return genom_.GetNumber(length); }
    Operation TestNextMove(long long energy, long long treshhold = 0)
    {
        energy_ = energy;
        treshhold_energy_ = 0;
        return nextMove(energy, false);
    }

    int testParseExpression(long long energy, long long treshhold = 0)
    {
        energy_ = energy;
        treshhold_energy_ = treshhold;
        int result = parseExpression();
        return result;
    }

    bool testParseBoolExpression(long long energy, long long treshhold = 0)
    {
        energy_ = energy;
        treshhold_energy_ = treshhold;
        bool result = parseBoolExpression();
        return result;
    }
    void testGoto(long long energy, long long treshhold = 0)
    {
        energy_ = energy;
        treshhold_energy_ = treshhold;
        // skip goto command in genom
        GetNumberFromGenom(CommandLength::OPERATION);
        // parse goto param
        parseGoto();
    }
    long long GetEnergy() const { return energy_; }
};

const long long START_ENERGY = 100;

void appender(GenomContainer& vec);
template <typename T_COMMAND, typename T_LENGTH, typename... T>
void appender(GenomContainer& vec, std::pair<T_COMMAND, T_LENGTH> command, T... args)
{
    vec.Append(static_cast<size_t>(command.first), Length(static_cast<size_t>(command.second)));
    appender(vec, args...);
}

#endif // COMMON_HPP
